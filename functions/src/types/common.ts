import { Request } from 'express'

export interface VerifiedRequest extends Request {
  userId?: string
}

export interface StandardRec {
  id: string
  createdBy: string
}