import admin from 'firebase-admin'
import { Response, Express } from 'express'
import { StandardRec, VerifiedRequest } from '../types/common'
import verifyToken from '../auth/verify-tokens'

export default (app: Express): void => {
  app.get('/api/encounters', verifyToken, async (req: VerifiedRequest, res: Response): Promise<void> => {
    const db = admin.firestore()
    console.log(`checking for encounters matching ${req.userId}`)
    const encounterMatches = await db.collection('encounters').where('createdBy', '==', req.userId).get()

    const encounters: Record<string, StandardRec> = {}
    if (!encounterMatches.empty) {
      encounterMatches.forEach(encounter => { 
        const encounterData = encounter.data() as StandardRec
        encounters[encounterData.id] = encounterData 
      })
    }

    res.status(200).send(encounters)
  })

  app.post('/api/encounter', verifyToken, async (req: VerifiedRequest, res: Response): Promise<void> => {
    const db = admin.firestore()

    console.log(`encounter with id ${req.body.id}`)
    const docRef = db.collection('encounters').doc(req.body.id)
    const existingEncounter = await docRef.get()

    // Make sure we're not overwriting someone elses encounter
    if (existingEncounter.exists) {
      const existingEncounterData = existingEncounter.data() as StandardRec
      if (existingEncounterData.createdBy !== req.userId) {
        res.status(403).send('Cant go editting another guys encounter, bit rude')
      }
    }

    docRef.set(req.body)

    res.status(200).send()
  })

  app.delete('/api/encounter', verifyToken, async (req: VerifiedRequest, res: Response) => {
    const { userId, body } = req

    const db = admin.firestore()
    const docRef = db.collection('encounters').doc(body.id)

    const encounter = await docRef.get()
    // Make sure this user has permission to delete this encounter
    if (encounter.exists) {
      const encounterData = encounter.data() as StandardRec
      if (encounterData.createdBy !== userId) {
        res.status(403).send('That aint ya encounter')
      } else {
        docRef.delete()
        res.status(200).send({ id: body.id })
      }
    } else {
      res.status(404).send('encounter does not exist')
    }
  })
}
