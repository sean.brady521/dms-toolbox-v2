import admin from 'firebase-admin'
import { Response, Express } from 'express'
import { StandardRec, VerifiedRequest } from '../types/common'
import verifyToken from '../auth/verify-tokens'

async function getSections (userId: string): Promise<StandardRec[]> {
  const db = admin.firestore()
  console.log(`checking for sections matching ${userId}`)
  const sectionMatches = await db.collection('sections').where('createdBy', '==', userId).get()

  const sections: StandardRec[] = []
  if (!sectionMatches.empty) {
    sectionMatches.forEach(section => { sections.push(section.data() as StandardRec) })
  }

  return sections
}

async function setSections (userId: string, sections: StandardRec[]): Promise<number> {
  const db = admin.firestore()

  for (const sec of sections) {
    const docRef = db.collection('sections').doc(sec.id)
    const existingSec = await docRef.get()
    if (existingSec.exists) {
      const secData = existingSec.data() as StandardRec
      if (secData.createdBy !== userId) {
        return 403
      }
    }
    docRef.set(sec)
  }

  return 200
}

async function deleteSection (userId: string, id: string): Promise<number> {
  const db = admin.firestore()

  const docRef = db.collection('sections').doc(id)
  const existingSec = await docRef.get()
  if (existingSec.exists) {
    const secData = existingSec.data() as StandardRec
    if (secData.createdBy !== userId) {
      return 403
    } else {
      docRef.delete()
    }
  }

  return 200
}

export default (app: Express): void => {
  app.get('/api/sections', verifyToken, async (req: VerifiedRequest, res: Response): Promise<void> => {
    if (!req.userId) return
    const sections = await getSections(req.userId)
    res.status(200).send(sections)
  })

  app.post('/api/sections', verifyToken, async (req: VerifiedRequest, res: Response): Promise<void> => {
    const secs = Array.isArray(req.body) ? req.body : [req.body]
    if(!req.userId) return

    const code = await setSections(req.userId, secs as StandardRec[])

    res.status(code).send()
  })

  app.delete('/api/section', verifyToken, async (req: VerifiedRequest, res: Response) => {
    const { userId, body } = req

    if (!userId) return

    const code = await deleteSection(userId, body.id)

    res.status(code).send()
  })
}
