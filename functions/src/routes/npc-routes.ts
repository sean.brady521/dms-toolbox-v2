import admin from 'firebase-admin'
import { Response, Express } from 'express'
import { StandardRec, VerifiedRequest } from '../types/common'
import verifyToken from '../auth/verify-tokens'

export default (app: Express): void => {
  app.get('/api/npcs', verifyToken, async (req: VerifiedRequest, res: Response): Promise<void> => {
    const db = admin.firestore()
    console.log(`checking for NPCs matching ${req.userId}`)
    const npcMatches = await db.collection('npcs').where('createdBy', '==', req.userId).get()

    const npcs: Record<string, StandardRec> = {}
    if (!npcMatches.empty) {
      npcMatches.forEach(npc => { 
        const npcData = npc.data() as StandardRec
        npcs[npcData.id] = npcData 
      })
    }

    res.status(200).send(npcs)
  })

  app.post('/api/npc', verifyToken, async (req: VerifiedRequest, res: Response): Promise<void> => {
    const db = admin.firestore()

    console.log(`npc with id ${req.body.id}`)
    const docRef = db.collection('npcs').doc(req.body.id)
    const existingNpc = await docRef.get()

    // Make sure we're not overwriting someone elses NPC
    if (existingNpc.exists) {
      const existingNpcData = existingNpc.data() as StandardRec
      if (existingNpcData.createdBy !== req.userId) {
        res.status(403).send('Cant go editting another guys NPC, bit rude')
      }
    }

    docRef.set(req.body)

    res.status(200).send()
  })

  app.delete('/api/npc', verifyToken, async (req: VerifiedRequest, res: Response) => {
    const { userId, body } = req

    const db = admin.firestore()
    const docRef = db.collection('npcs').doc(body.id)

    const npc = await docRef.get()
    // Make sure this user has permission to delete this NPC
    if (npc.exists) {
      const npcData = npc.data() as StandardRec
      if (npcData.createdBy !== userId) {
        res.status(403).send('That aint ya NPC')
      } else {
        docRef.delete()
        res.status(200).send({ id: body.id })
      }
    } else {
      res.status(404).send('NPC does not exist')
    }
  })
}
