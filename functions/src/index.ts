const functions = require('firebase-functions') //eslint-disable-line

import express from 'express'

import admin from 'firebase-admin'

import cors from 'cors'

import npcRoutes from './routes/npc-routes'

import sectionRoutes from './routes/section-routes'
import encounterRoutes from './routes/encounter-routes'

const app = express()

app.use(cors({ origin: true }))
app.use(express.json())
app.use(express.urlencoded())

admin.initializeApp()

encounterRoutes(app)
npcRoutes(app)
sectionRoutes(app)

exports.app = functions.https.onRequest(app)