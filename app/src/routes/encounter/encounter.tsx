import React, { useEffect, useState } from 'react'

import InfoBlock from 'components/info-block'
import Button from 'components/button'
import { InfoBlockItem } from 'components/info-block/info-block'
import InfoPage from 'components/info-page'
import MonsterSheet from 'components/monster-sheet'
import { IEncounter } from 'types/encounters'
import { MonsterData } from 'types/monster'
import styles from './encounter.module.scss'


export interface EncounterProps {
  encounter: IEncounter
  creatures: Record<string, MonsterData>
  loadingCreatures: boolean
  onDelete: () => void
  fetchMonsters: (creatures: string[]) => void
  goToEncountersPage: () => void
  randomiseEncounter: (encounter: IEncounter) => void
}

const Encounter = (props: EncounterProps): JSX.Element => {
  const { 
    encounter,
    creatures,
    loadingCreatures,
    onDelete,
    fetchMonsters,
    goToEncountersPage,
    randomiseEncounter
  } = props

  const [monsterSheet, setMonsterSheet] = useState<string | null>(null)

  console.log({encounter})

  const minutesSinceCreate = (Date.now() - encounter.createdAt) / 60000
  const showRandomise = minutesSinceCreate < 5 && !encounter.manual && encounter.config

  useEffect(() => {
    if (encounter.creatures.every(mon => creatures[mon.creature])) {
      return
    }
    fetchMonsters(encounter.creatures.map(mon => mon.creature))
  }, [encounter])

  function getMonsterSummary (monster: string): InfoBlockItem[] {
    const monsterData = creatures[monster] || {}

    return [
      {
        icon: 'bolt',
        content: `${monsterData.challenge_rating ?? 'N/A'}`,
        tooltip: 'Challenge rating'
      },
      {
        icon: 'heart',
        content: `${monsterData.hit_points ?? 'N/A'}`,
        tooltip: 'Hit points'
      },
      {
        icon: 'shield',
        content: `${monsterData.armor_class ?? 'N/A'}`,
        tooltip: 'Armour Class'
      },
      {
        icon: 'fighter jet',
        content: `${monsterData.speed?.walk ?? 'N/A'}`,
        tooltip: 'Movement speed'
      },
    ]
  }

  return (
    <InfoPage
      onBack={goToEncountersPage} 
      title={encounter.name || 'Anonymous encounter'}
      menuItems={[{icon: 'trash alternate', label: 'Delete', onClick: onDelete}]}
    >
      <MonsterSheet 
        open={monsterSheet !== null} 
        onClose={() => setMonsterSheet(null)}
        monster={monsterSheet ? creatures[monsterSheet] ?? null : null}
      />
      <div className={styles.blocksWrap}>
        <div className={styles.creatureBlocks}>
          {encounter.creatures.map(mon => (
            <InfoBlock 
              key={mon.creature}
              items={getMonsterSummary(mon.creature)} 
              showPlaceholder={loadingCreatures}
              badge={mon.count > 1 ? `${mon.count}` : undefined}
              className={styles.creatureCard}
              contentClassName={styles.creatureCardContent}
              title={creatures[mon.creature]?.name || mon.creature}
              onClick={() => setMonsterSheet(mon.creature)}
            />

          ))}
        </div>
        <div className={styles.metaBlocks}>
          {encounter.relationship &&
            <InfoBlock 
              className={styles.textBlock}
              title='Relationship' 
            >
              <div className={styles.textBlockInner}>
                {encounter.relationship}
              </div>
            </InfoBlock>
          }
          {encounter.personality &&
            <InfoBlock 
              className={styles.textBlock}
              title='Personality' 
            >
              <div className={styles.textBlockInner}>
                {encounter.personality || 'No personality'}
              </div>
            </InfoBlock>
          }
        </div>
        {showRandomise &&
          <div className={styles.reroll}>
            <span className={styles.question}>Not a fan? </span>
            <Button className={styles.randomise} onClick={() => randomiseEncounter(encounter)}>
              Randomise
            </Button>
          </div>
        }
      </div>
    </InfoPage>
  )
}

export default Encounter