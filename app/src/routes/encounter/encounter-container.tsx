import React from 'react'
import { connect } from 'react-redux'

import Encounter, { EncounterProps } from './encounter'
import { StoreState } from 'types/common'
import { Dispatch } from 'redux'
import { fetchCreatures, postEncounter, removeEncounter } from 'store/encounters/encounters-actions'
import { RouteComponentProps, useParams } from 'react-router'
import { replace } from 'connected-react-router'
import { generateRandomEncounter } from 'utils/encounter-generator/generator'
import { EncounterConfig, IEncounter } from 'types/encounters'

export const EncounterContainer = (props: EncounterProps): JSX.Element => {
  return <Encounter {...props} />
}

export function mapStateToProps(state: StoreState, ownProps: RouteComponentProps<{ id: string }>): Pick<EncounterProps, 'encounter' | 'creatures' | 'loadingCreatures'> {
  const { encounters } = state

  const { id } = ownProps.match.params

  return {
    encounter: encounters.encounters[id],
    creatures: encounters.creatureCache,
    loadingCreatures: encounters.fetchingCreatures
  }
}

type DispatchProps = 'onDelete' | 'goToEncountersPage' | 'fetchMonsters' | 'randomiseEncounter'
export function mapDispatchToProps(dispatch: Dispatch, ownProps: RouteComponentProps<{ id: string }>): Pick<EncounterProps, DispatchProps> {
  const { id } = ownProps.match.params
  return {
    onDelete: () => {
      removeEncounter(id)(dispatch)
    },
    goToEncountersPage: () => {
      dispatch(replace('/encounters'))
    },
    fetchMonsters: (creatures: string[]) => {
      fetchCreatures(creatures)(dispatch)
    },
    randomiseEncounter: (encounter: IEncounter) => {
      if (!encounter.config) return
      const { numPcs, avgLvl, difficulty, environment } = encounter.config
      if (!difficulty || !environment) return

      const rand = generateRandomEncounter(numPcs, avgLvl, difficulty, environment)
      postEncounter({ ...encounter, ...rand })(dispatch)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EncounterContainer)