import React from 'react'
import { connect } from 'react-redux'

import Npc, { NpcProps } from './npc'
import { StoreState } from 'types/common'
import { INpc } from 'types/npcs'
import { Dispatch } from 'redux'
import { removeNpc, postNpc } from 'store/npcs/npcs-actions'
import { RouteComponentProps } from 'react-router'
import { replace } from 'connected-react-router'
import { generateRandomNpc } from 'utils/npc-generator/npc-generator'

export const NpcContainer = (props: NpcProps): JSX.Element => {
  return <Npc {...props} />
}

export function mapStateToProps(state: StoreState, ownProps: RouteComponentProps<{ id: string }>): Pick<NpcProps, 'npc'> {
  const { npcs } = state

  const { id } = ownProps.match.params

  return {
    npc: npcs.npcs[id]
  }
}

export function mapDispatchToProps(dispatch: Dispatch, ownProps: RouteComponentProps<{ id: string }>): Pick<NpcProps, 'onDelete' | 'goToNpcsPage' | 'randomiseNpc'> {
  const { id } = ownProps.match.params
  return {
    onDelete: () => {
      removeNpc(id)(dispatch)
    },
    goToNpcsPage: () => {
      dispatch(replace('/npcs'))
    },
    randomiseNpc: (npc: INpc) => {
      const rand = generateRandomNpc(npc.config)
      postNpc({ ...npc, ...rand })(dispatch)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NpcContainer)