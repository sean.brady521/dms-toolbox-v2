import InfoBlock from 'components/info-block'
import InfoPage from 'components/info-page'
import React, { useState } from 'react'
import { INpc } from 'types/npcs'
import { Icon, Popup, SemanticICONS } from 'semantic-ui-react'
import styles from './npc.module.scss'
import { InfoBlockItem } from 'components/info-block/info-block'
import AbilityScore from 'components/ability-score'
import { abilityScores } from 'constants/common'
import { rerollQuestions } from 'constants/common'
import { randSelect } from 'utils/rand'
import Button from 'components/button'

let question: string | undefined

export interface NpcProps {
  npc: INpc
  onDelete: () => void
  goToNpcsPage: () => void
  randomiseNpc: (npc: INpc) => void
}

function getPhysicalAttribs (npc: INpc): InfoBlockItem[] {
  const descItems = npc.description?.split('\n') || []
  const icons: SemanticICONS[] = ['eye', 'hand paper outline', 'male', 'meh' ,'star', 'star']
  const attribs: InfoBlockItem[] = []
  for (let i = 1; i < descItems.length; i++) {
    if (descItems[i] === '') continue
    attribs.push({
      icon: icons[i - 1],
      content: descItems[i],
    })
  }
  return attribs
}

function getKeyInfo (npc: INpc): InfoBlockItem[] {
  return [
    {
      icon: 'birthday',
      content: `${npc.age} years old`
    },
    {
      icon: 'male',
      content: `${npc.gender} ${npc.race}`
    },
    {
      icon: 'suitcase',
      content: npc.class || 'No occupation'
    },
    {
      icon: 'blind',
      content: npc.alignment || 'No alignment'
    },
  ]
}

function getVoice (npc: INpc): InfoBlockItem[] {
  const { pace, physicalQuirk, pitch, speechQuirk, tone } = npc.voice || {}
  return [
    {
      icon: 'fast forward',
      content: `${pace || 'Average'} pace`
    },
    {
      icon: 'gitter',
      content: `${pitch || 'Average'} pitch`
    },
    {
      icon: 'microphone',
      content: `${tone || 'Average'} tone`
    },
    {
      icon: 'user',
      content: `${physicalQuirk || 'None'}`
    },
    {
      icon: 'volume up',
      content: `${speechQuirk || 'None'}`
    },
  ]
}

function getPersonalityTraits (npc: INpc): InfoBlockItem[] {
  const perItems = npc.personality?.split('\n') || []
  const attribs: InfoBlockItem[] = []
  for (let i = 0; i < perItems.length; i++) {
    if (perItems[i] === '') continue
    attribs.push({
      icon: i === 0 ? 'chess king' : 'cogs',
      content: perItems[i],
    })
  }
  return attribs
}

const Npc = (props: NpcProps): JSX.Element => {
  const { 
    npc,
    onDelete,
    goToNpcsPage,
    randomiseNpc
  } = props

  const minutesSinceCreate = (Date.now() - npc.createdAt) / 60000
  if (minutesSinceCreate < 5 && !question) {
    question = randSelect(rerollQuestions)
  }

  const handleChangeStat = (abbrev: string, value: number) => {
    console.log({abbrev, value})
  }

  return (
    <InfoPage
      onBack={goToNpcsPage} 
      title={npc.name || 'Mr. No Name'}
      menuItems={[{icon: 'trash alternate', label: 'Delete', onClick: onDelete}]}
    >
      <div className={styles.blocksWrap}>
        <InfoBlock 
          className={styles.keyInfo}
          contentClassName={styles.keyInfoContent}
          title='Key Info' 
          items={getKeyInfo(npc)}
        />
        <InfoBlock 
          className={styles.voice}
          contentClassName={styles.voiceContent}
          title='Voice' 
          items={getVoice(npc)}
        />
        <InfoBlock 
          className={styles.hook}
          title='Plot Hook' 
        >
          <div className={styles.hookInner}>
            {npc.hook || 'No hook'}
          </div>
        </InfoBlock>
        <InfoBlock 
          className={styles.pandp}
          contentClassName={styles.pandpContent}
          title='Physical Description' 
          items={getPhysicalAttribs(npc)}
        />
        <InfoBlock 
          className={styles.pandp}
          contentClassName={styles.pandpContent}
          title='Personality Traits' 
          items={getPersonalityTraits(npc)}
        />
        <InfoBlock className={styles.stats} title='Ability Scores'>
          <div className={styles.statsContent}>
            {abilityScores.map(stat => (
              <AbilityScore 
                name={`${stat.label}`}
                key={stat.abbrev}
                className={styles.score}
                value={npc.abilities?.[stat.abbrev] || 10} 
                // edittable
                onChange={newVal => handleChangeStat(stat.abbrev, parseInt(newVal))}
              />
            ))}
          </div>
        </InfoBlock>
      </div>
      {question &&
        <div className={styles.reroll}>
          <span className={styles.question}>{question}</span>
          <Button className={styles.randomise} onClick={() => randomiseNpc(npc)}>
            Randomise
          </Button>
        </div>
      }
    </InfoPage>
  )
}

export default Npc