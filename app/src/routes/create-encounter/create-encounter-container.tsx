import React from 'react'
import { connect } from 'react-redux'

import CreateEncounter, { CreateEncounterProps } from './create-encounter'
import { ISection, StoreState } from 'types/common'
import { Dispatch } from 'redux'
import monsters from 'utils/encounter-generator/monsters.json'
import { ECreature, EncounterConfig } from 'types/encounters'
import { getRootSection } from 'utils/sections'
import { fetchCreatures, postEncounter } from 'store/encounters/encounters-actions'
import { addNewEncounterMeta } from 'utils/encounter-generator/generator'
import { addRecToSection } from 'store/sections/sections-actions'
import { replace } from 'connected-react-router'

export const CreateEncounterContainer = (props: CreateEncounterProps): JSX.Element => {
  return <CreateEncounter {...props} />
}

type StateProps = Pick<CreateEncounterProps, 'simpleCreatureList' | 'creatureMap' | 'loadingCreature' | 'targetSection'>
export function mapStateToProps(state: StoreState): StateProps {
  const { encounters, sections } = state

  return {
    targetSection: getRootSection('encounters', sections.sections),
    simpleCreatureList: monsters,
    creatureMap: encounters.creatureCache,
    loadingCreature: encounters.fetchingCreatures
  }
}

type DispatchProps = Pick<CreateEncounterProps, 'createEncounter' | 'fetchCreature' | 'goToEncountersPage'>
export function mapDispatchToProps(dispatch: Dispatch): DispatchProps {
  return {
    createEncounter: (name: string, creatures: ECreature[], config: EncounterConfig, section: ISection) => {
      const encounter = { 
        ...addNewEncounterMeta({ name, creatures, config }),
        manual: true
      }
      postEncounter(encounter)(dispatch)
      addRecToSection(encounter.id, section)(dispatch)
    },
    fetchCreature: (creature: string) => {
      fetchCreatures([creature])(dispatch)
    },
    goToEncountersPage: () => {
      dispatch(replace('/encounters'))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateEncounterContainer)