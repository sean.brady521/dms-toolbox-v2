import React, { useState } from 'react'
import FilterChip from 'components/filter-chip'
import Checkbox from 'components/checkbox'

import styles from './filter-environment.module.scss'
import { getMonsterEnvs } from 'utils/monsters'
import { uppercaseFirstLetter } from 'utils/string'

type State = string[]

export interface FilterTypeProps {
  savedValue?: State
  onSave: (state?: State) => void
}

const FilterType = (props: FilterTypeProps): JSX.Element => {
  const { savedValue, onSave } = props

  const [editState, setEditState] = useState<State | null>(savedValue || null)

  const handleSave = () => {
    if (!editState?.length || editState === null) onSave()
    else onSave(editState)

    setEditState(null)
  }

  const handleToggle = (environment: string): void => {
    let newState
    if (editState?.includes(environment)) {
      newState = editState.filter(t => t !== environment)
    } else {
      newState = [...(editState || []), environment]
    }
    setEditState(newState)
  }

  const environments = getMonsterEnvs()

  return (
    <FilterChip
      label='Environment'            
      displayValue={savedValue ? savedValue.join(', ') : ''}
      onSave={handleSave}
      contentClassName={styles.content}
      onOpen={() => setEditState(savedValue || null)}
      onClose={() => setEditState(null)}
      onClear={() => setEditState(null)}
      active={!!savedValue?.length} 
    >
      <div className={styles.listWrap}>
        {environments.map(environment => (
          <Checkbox 
            key={environment}
            className={styles.checkbox}
            checked={!!editState?.includes(environment)} 
            onToggle={() => handleToggle(environment)} 
            label={uppercaseFirstLetter(environment)} 
          />
        ))}
      </div>
    </FilterChip>
  )
}

export default FilterType