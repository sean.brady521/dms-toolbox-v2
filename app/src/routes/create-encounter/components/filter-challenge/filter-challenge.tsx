import React, { useState } from 'react'
import FilterChip from 'components/filter-chip'
import Slider from 'components/slider'
import Input from 'components/input'
import { minMaxString } from 'utils/query'

import styles from './filter-challenge.module.scss'

const MIN_CR = 0
const MAX_CR = 30

interface FormState {
  min: number | ''
  max: number | ''
}

interface SubmitState {
  min: number
  max: number
}

export interface FilterChallengeProps {
  savedValue?: SubmitState
  onSave: (state: SubmitState) => void
}

const FilterChallenge = (props: FilterChallengeProps): JSX.Element => {
  const { savedValue, onSave } = props

  const [editState, setEditState] = useState<FormState | null>(savedValue || null)

  const handleSave = () => {
    if (editState === null) return
    const submitState: SubmitState = {
      min: editState?.min || MIN_CR,
      max: editState?.max || MAX_CR
    }
    onSave(submitState)
    setEditState(null)
  }

  return (
    <FilterChip
      label='Challenge'            
      displayValue={((savedValue?.min || 0) > MIN_CR || (savedValue?.max || MAX_CR) < MAX_CR) 
        ? minMaxString(savedValue || {}) ?? '' 
        : ''
      }
      contentClassName={styles.content}
      onSave={handleSave}
      onClear={() => setEditState({min: MIN_CR, max: MAX_CR})}
      onOpen={() => setEditState(savedValue || null)}
      onClose={() => setEditState(null)}
      active={!!savedValue && (savedValue.min !== MIN_CR || savedValue.max !== MAX_CR)} 
    >
      <Slider 
        value={[editState?.min || MIN_CR, editState?.max || MAX_CR]}
        min={MIN_CR}
        max={MAX_CR}
        marks={[0, 10, 20, 30].map(value => ({ value, label: `${value}` }))}
        className={styles.slider}
        onChange={newValue => {
          const [min, max] = newValue as number[]
          setEditState({ min, max })
        }}
      />
      <div className={styles.rangeWrap}>
        <div className={styles.inputWrap}>
          <label className={styles.label}>
            min rating
          </label>
          <Input 
            value={editState?.min ?? ''}
            type='number'
            className={styles.rangeInput}
            onBlur={() => {
              if (editState?.min === undefined || editState?.min === '' || editState?.min < MIN_CR || editState?.min > editState?.max) {
                setEditState({ min: editState?.min || MIN_CR, max: MAX_CR })
              }
            }}
            onChange={(_, { value }) => { 
              const numberVal: number = parseInt(value)
              const parsedVal = isNaN(numberVal) ? '' : numberVal

              setEditState({ min: parsedVal, max: editState?.max ?? MAX_CR})}
            }
          />
        </div>
        <span className={styles.dash}>
        -
        </span>
        <div className={styles.inputWrap}>
          <label className={styles.label} htmlFor='input_max'>
            max rating
          </label>
          <Input 
            id='input_max'
            value={editState?.max ?? ''}
            type='number'
            className={styles.rangeInput}
            onBlur={() => {
              if (editState?.max === undefined || editState?.max === '' || editState?.max > MAX_CR || editState?.max < editState?.min) {
                setEditState({ min: editState?.min || MIN_CR, max: MAX_CR })
              }
            }}
            onChange={(_, { value }) => { 
              const numberVal: number = parseInt(value)
              const parsedVal = isNaN(numberVal) ? '' : numberVal

              setEditState({ min: editState?.min || MIN_CR, max: parsedVal})}
            }
          />
        </div>
      </div>
    </FilterChip>
  )
}

export default FilterChallenge