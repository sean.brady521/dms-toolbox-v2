import React, { useState } from 'react'
import FilterChip from 'components/filter-chip'
import Slider from 'components/slider'
import Input from 'components/input'

import styles from './filter-size.module.scss'
import { MonsterSize } from 'constants/enums'

interface FormState {
  min: MonsterSize | ''
  max: MonsterSize | ''
}

interface SubmitState {
  min: MonsterSize
  max: MonsterSize
}

export interface FilterSizeProps {
  savedValue?: SubmitState
  onSave: (state: SubmitState) => void
}

const FilterSize = (props: FilterSizeProps): JSX.Element => {
  const { savedValue, onSave } = props

  const [editState, setEditState] = useState<FormState | null>(savedValue || null)

  const handleSave = () => {
    if (editState === null) return
    const submitState: SubmitState = {
      min: editState?.min || MonsterSize.Tiny,
      max: editState?.max || MonsterSize.Gargantuan
    }
    onSave(submitState)
    setEditState(null)
  }

  return (
    <FilterChip
      label='Size'            
      onSave={handleSave}
      onClear={() => setEditState({min: MonsterSize.Tiny, max: MonsterSize.Gargantuan})}
      onOpen={() => setEditState(savedValue || null)}
      onClose={() => setEditState(null)}
      contentClassName={styles.content}
      active={!!savedValue && (savedValue.min !== MonsterSize.Tiny || savedValue.max !== MonsterSize.Gargantuan)} 
    >
      <Slider 
        value={[editState?.min || MonsterSize.Tiny, editState?.max || MonsterSize.Gargantuan]}
        className={styles.slider}
        min={MonsterSize.Tiny}
        max={MonsterSize.Gargantuan}
        marks={[
          { value: MonsterSize.Tiny, label: 'Tiny' },
          { value: MonsterSize.Gargantuan, label: <span>Gargantuan&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span> }
        ]}
        onChange={newValue => {
          const [min, max] = newValue as number[]
          setEditState({ min, max })
        }}
      />
      <div className={styles.rangeWrap}>
        <div className={styles.valueWrap}>
          <label className={styles.label}>
            min size
          </label>
          <div className={styles.sizeVal}>
            {MonsterSize[editState?.min || MonsterSize.Tiny]}
          </div>
        </div>
        <span className={styles.dash}>
        -
        </span>
        <div className={styles.valueWrap}>
          <label className={styles.label} htmlFor='input_max'>
            max size
          </label>
          <div className={styles.sizeVal}>
            {MonsterSize[editState?.max || MonsterSize.Gargantuan]}
          </div>
        </div>
      </div>
    </FilterChip>
  )
}

export default FilterSize