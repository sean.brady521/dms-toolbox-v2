import React, { useState } from 'react'
import FilterChip from 'components/filter-chip'
import Checkbox from 'components/checkbox'

import styles from './filter-type.module.scss'
import { getMonsterTypes } from 'utils/monsters'
import { uppercaseFirstLetter } from 'utils/string'

type State = string[]

export interface FilterTypeProps {
  savedValue?: State
  onSave: (state?: State) => void
}

const FilterType = (props: FilterTypeProps): JSX.Element => {
  const { savedValue, onSave } = props

  const [editState, setEditState] = useState<State | null>(savedValue || null)

  const handleSave = () => {
    if (!editState?.length || editState === null) onSave()
    else (onSave(editState))

    setEditState(null)
  }

  const handleToggle = (type: string): void => {
    let newState
    if (editState?.includes(type)) {
      newState = editState.filter(t => t !== type)
    } else {
      newState = [...(editState || []), type]
    }
    setEditState(newState)
  }

  const types = getMonsterTypes()

  return (
    <FilterChip
      label='Type'            
      displayValue={savedValue ? savedValue.join(', ') : ''}
      onSave={handleSave}
      contentClassName={styles.content}
      onOpen={() => setEditState(savedValue || null)}
      onClose={() => setEditState(null)}
      onClear={() => setEditState([])}
      active={!!savedValue?.length} 
    >
      <div className={styles.listWrap}>
        {types.map(type => (
          <Checkbox 
            key={type}
            className={styles.checkbox}
            checked={!!editState?.includes(type)} 
            onToggle={() => handleToggle(type)} 
            label={uppercaseFirstLetter(type)} 
          />
        ))}
      </div>
    </FilterChip>
  )
}

export default FilterType