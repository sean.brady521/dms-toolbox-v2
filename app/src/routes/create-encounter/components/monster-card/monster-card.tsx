import React, { useEffect, useState } from 'react'
import styles from './monster-card.module.scss'
import classNames from 'classnames'
import NumberStepper from 'components/number-stepper'
import { Icon } from 'semantic-ui-react'

export interface MonsterCardProps {
  name: string
  className?: string
  desc: string
  cr: number
  xp: number
  count: number
  onChangeCount: (count: number) => void
  onRemove: () => void
}

const MonsterCard = (props: MonsterCardProps): JSX.Element => {
  const {
    name,
    className,
    desc,
    cr,
    xp,
    count,
    onChangeCount,
    onRemove
  } = props

  return (
    <div 
      className={classNames(styles.root, className)}
    >
      <div className={styles.crossWrap} onClick={onRemove}>
        <Icon name='close' className={styles.cross} />
      </div>
      <div className={styles.cardInfo}>
        <div className={styles.nameAndDesc}>
          <h3 className={styles.name}>
            {name}
          </h3>
          <div className={styles.desc}>
            {desc}
          </div>
        </div>
        <div className={styles.stats}>
          <div className={styles.keyVal}>
            <span className={styles.key}>CR: </span>
            <span className={styles.val}>{cr}</span>
          </div>
          <div>
            <span className={styles.key}>XP: </span>
            <span className={styles.val}>{xp}</span>
          </div>
        </div>
      </div>
      <div className={styles.counter}>
        <NumberStepper 
          value={count}  
          onChange={onChangeCount}
          vertical
        />
      </div>

    </div>
  )
}

export default MonsterCard