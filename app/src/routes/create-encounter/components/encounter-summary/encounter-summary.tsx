import React from 'react'

import NumberStepper from 'components/number-stepper'
import Button from 'components/button'

import { EncounterState } from 'routes/create-encounter/create-encounter'
import { Divider } from 'semantic-ui-react'
import { SimpleMonster } from 'types/monster'
import { ECreature } from 'types/encounters'
import { difficultyToName, getEncounterCrs, getEncounterDifficulty, getModifiedEncounterXp, getXpBoundaries } from 'utils/encounter'
import MonsterCard from '../monster-card'

import styles from './encounter-summary.module.scss'

export interface EncounterSummaryProps {
  creatureMap: Record<string, SimpleMonster>
  encounter: EncounterState
  onChange: (newState: EncounterState) => void
  onSubmit: () => void
}

const EncounterSummary = (props: EncounterSummaryProps): JSX.Element => {
  const { encounter, creatureMap, onChange, onSubmit } = props

  const { creatures, config } = encounter

  const creatureCrs = getEncounterCrs(creatures, creatureMap)

  const rawXp = creatures.reduce((acc, c) => acc + creatureMap[c.creature].xp * c.count, 0)

  const difficulty = getEncounterDifficulty(creatureCrs, config.numPcs, config.avgLvl)
  const modifiedXp = getModifiedEncounterXp(creatureCrs, config.numPcs)

  const xpBoundaries = getXpBoundaries(config.avgLvl, config.numPcs)

  const handleRemove = (mon: string) => {
    const newCreatures = encounter.creatures.filter(crConf => crConf.creature !== mon)
    onChange({ ...encounter, creatures: newCreatures })
  }

  return (
    <div className={styles.root}>
      <h2 className={styles.header}>
        Encounter Summary
      </h2>
      <Divider className={styles.divider} />
      <div className={styles.configWrap}>
        <div className={styles.configItemWrap}>
          <div className={styles.configLabel}>
            Avg player level
          </div>
          <div className={styles.stepperWrap}>
            <NumberStepper 
              value={config.avgLvl}
              onChange={num => onChange({ ...encounter, config: { ...config, avgLvl: num } })}
            />
          </div>
        </div>
        <div className={styles.configItemWrap}>
          <div className={styles.configLabel}>
            Number of players
          </div>
          <div className={styles.stepperWrap}>
            <NumberStepper 
              value={config.numPcs}
              onChange={num => onChange({ ...encounter, config: { ...config, numPcs: num } })}
            />
          </div>
        </div>
      </div> 
      <div className={styles.difficultyWrap}>
        <div className={styles.lhsWrap}>
          <div className={styles.labelValWrap}>
            <div className={styles.difficultyLabel}>
              Difficulty
            </div>
            <div className={styles.difficultyVal}>
              {difficultyToName[difficulty]}
            </div>
          </div>
          <div className={styles.labelValWrap}>
            <div className={styles.difficultyLabel}>
              Raw XP
            </div>
            <div className={styles.difficultyVal}>
              {rawXp}
            </div>
          </div>
          <div className={styles.labelValWrap}>
            <div className={styles.difficultyLabel}>
              Final Xp
            </div>
            <div className={styles.difficultyVal}>
              {modifiedXp}
            </div>
          </div>
        </div>
        <div className={styles.rhsWrap}>
          {xpBoundaries.map(boundary => (
            <div key={boundary.label} className={styles.keyValWrap}>
              <div className={styles.difficultyLabel}>
                {boundary.label}
              </div>
              <div className={styles.difficultyVal}>
                {`${boundary.value} xp`} 
              </div>
            </div>
          ))}
        </div>
      </div>
      <div className={styles.monsterWrap}>
        {creatures.map(creatureConf => { 
          const mon = creatureMap[creatureConf.creature]
          return (
            <MonsterCard 
              key={mon.index} 
              className={styles.monsterCard}
              name={mon.name}
              desc={`${mon.size} ${mon.type}`}
              cr={mon.cr}
              xp={mon.xp}
              count={creatureConf.count}
              onRemove={() => handleRemove(mon.index)}
              onChangeCount={(count: number) => {
                if (count === 0) {
                  handleRemove(mon.index)
                  return
                }
              
                const newCreatures = creatures.map(crConf => crConf.creature === mon.index 
                  ? { creature: crConf.creature, count } as ECreature
                  : crConf
                )

                onChange({ ...encounter, creatures: newCreatures })
              }}
            />
          )
        })}
      </div>
      <div className={styles.buttonWrap}>
        <Button 
          className={styles.createButton} 
          disabled={creatures.length === 0}
          onClick={onSubmit}
        >
          Create encounter
        </Button>
      </div>
    </div>
  )
}

export default EncounterSummary