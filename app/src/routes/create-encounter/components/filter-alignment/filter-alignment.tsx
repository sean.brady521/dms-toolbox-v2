import React, { useState } from 'react'
import FilterChip from 'components/filter-chip'

import styles from './filter-alignment.module.scss'
import ChipSelect from 'components/chip-select'
import { uppercaseFirstLetter } from 'utils/string'

const lawVsChaos = ['lawful', 'neutral', 'chaotic'] as const
const goodVsEvil = ['good', 'neutral', 'evil'] as const

type LawVsChaos = typeof lawVsChaos[number]
type GoodVsEvil = typeof goodVsEvil[number]

interface State {
  lawVsChaos: LawVsChaos[]
  goodVsEvil: GoodVsEvil[]
}

export interface FilterAlignmentProps {
  savedValue?: State
  onSave: (state: State) => void
}

const FilterAlignment = (props: FilterAlignmentProps): JSX.Element => {
  const { savedValue, onSave } = props

  const [editState, setEditState] = useState<State | null>(savedValue || null)

  const handleSave = () => {
    if (editState === null) return
    onSave(editState)
    setEditState(null)
  }

  return (
    <FilterChip
      label='Alignment'            
      onSave={handleSave}
      onClear={() => setEditState({ lawVsChaos: [], goodVsEvil: [] })}
      contentClassName={styles.content}
      onOpen={() => setEditState(savedValue || null)}
      onClose={() => setEditState(null)}
      active={!!savedValue && (savedValue.lawVsChaos.length > 0 || savedValue.goodVsEvil.length > 0)} 
    >
      <div className={styles.sectionWrap}>
        <div className={styles.sectionLabel}>
          Law vs. Chaos
        </div>
        <ChipSelect
          className={styles.chipSelect}
          selected={editState?.lawVsChaos || []}
          chipClassName={styles.chip}
          options={lawVsChaos.map(op => ({value: op, label: uppercaseFirstLetter(op)}))}
          onChange={selected => setEditState({ 
            lawVsChaos: selected as LawVsChaos[], 
            goodVsEvil: editState?.goodVsEvil || [] 
          })}
        />
      </div>
      <div className={styles.sectionWrap}>
        <div className={styles.sectionLabel}>
          Good vs. Evil
        </div>
        <ChipSelect
          className={styles.chipSelect}
          selected={editState?.goodVsEvil || []}
          chipClassName={styles.chip}
          options={goodVsEvil.map(op => ({value: op, label: uppercaseFirstLetter(op)}))}
          onChange={selected => setEditState({ 
            goodVsEvil: selected as GoodVsEvil[], 
            lawVsChaos: editState?.lawVsChaos || [] 
          })}
        />
      </div>
    </FilterChip>
  )
}

export default FilterAlignment