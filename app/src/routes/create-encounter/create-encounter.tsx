import React, { useEffect, useRef, useState } from 'react'
import pick from 'lodash.pick'
import { Button, Icon } from 'semantic-ui-react'
import { MonsterData, SearchQuery, SimpleMonster } from 'types/monster'
import { ECreature, EncounterConfig, IEncounter } from 'types/encounters'
import { ISection } from 'types/common'

import Input from 'components/input'

import styles from './create-encounter.module.scss'
import FilterType from './components/filter-type'
import FilterChallenge from './components/filter-challenge'
import FilterEnvironment from './components/filter-environment'
import FilterSize from './components/filter-size'
import FilterAlignment from './components/filter-alignment'
import SortableTable from 'components/sortable-table'

import monsterList from 'utils/encounter-generator/monsters.json'
import { selectFilteredMonsters } from 'utils/monsters'
import EncounterSummary from './components/encounter-summary'
import MonsterSheet from 'components/monster-sheet'
import IconButton from 'components/icon-button'

export type EncounterState = Pick<IEncounter, 'name' | 'config' | 'creatures' | 'personality' | 'relationship'>

const initState: EncounterState = {
  name: 'Untitled encounter',
  config: {
    avgLvl: 3,
    numPcs: 4
  },
  creatures: []
}

function getCreatureMap (creatures: string[]): Record<string, SimpleMonster> {
  if (!creatures.length) return {}

  const dataMap: Record<string, SimpleMonster> = {}
  for (const creature of creatures) {
    const data = monsterList.find(mon => mon.index === creature)

    if (data) {
      dataMap[creature] = data
    }
  }

  return dataMap
}

export interface CreateEncounterProps {
  targetSection: ISection
  simpleCreatureList: SimpleMonster[]
  creatureMap: Record<string, MonsterData>
  loadingCreature: boolean
  fetchCreature: (creature: string) => void
  createEncounter: (name: string, creatures: ECreature[], config: EncounterConfig, section: ISection) => void
  goToEncountersPage: () => void
}

const CreateEncounter = (props: CreateEncounterProps): JSX.Element => {
  const { 
    targetSection, 
    loadingCreature, 
    creatureMap, 
    fetchCreature, 
    createEncounter, 
    goToEncountersPage 
  } = props

  const [query, setQuery] = useState<SearchQuery>({})
  const [encounterState, setEncounterState] = useState<EncounterState>(initState)
  const [monsterSheet, setMonsterSheet] = useState<string | null>(null)

  useEffect(() => {
    if (!monsterSheet || creatureMap[monsterSheet]) return
    fetchCreature(monsterSheet)
  }, [monsterSheet])

  const availableMonsters = selectFilteredMonsters(monsterList, query)

  const nameRef = useRef<HTMLInputElement>(null)

  const handleSubmitName = () => {
    if (encounterState.name === '') setEncounterState({...encounterState, name: 'Untitled encounter'})
    nameRef.current?.blur()
  }

  return (
    <div className={styles.root}>
      <MonsterSheet 
        open={monsterSheet !== null} 
        onClose={() => { 
          setMonsterSheet(null)
        }}
        loading={loadingCreature}
        monster={monsterSheet ? creatureMap[monsterSheet] ?? null : null}
      />
      <div className={styles.lhsWrap}>
        <div className={styles.topSec}>
          <div className={styles.backAndTitle}>
            <div className={styles.backWrap} onClick={goToEncountersPage}>
              <Icon name='chevron left' className={styles.backIcon} />
            </div>
            <input 
              className={styles.title} 
              value={encounterState.name} 
              ref={nameRef}
              onChange={e => setEncounterState({...encounterState, name: e.currentTarget.value})}
              onKeyDown={e => {
                if (e.key === 'Enter' || e.keyCode === 13) {
                  handleSubmitName()
                }
              }}
              onBlur={() => { 
                handleSubmitName()
              }}
            /> 
          </div>
        </div>
        <div className={styles.selectionWrap}>
          <div className={styles.queryWrap}>
            <Input 
              icon='search' 
              placeholder='Search "adult green dragon"' 
              iconPosition='left' 
              value={query.q}
              onChange={(_, { value }) => setQuery({...query, q: value})} 
              className={styles.search}
            />
            <FilterChallenge
              savedValue={query.challenge} 
              onSave={state => setQuery({ ...query, challenge: state })}
            />
            <FilterType 
              savedValue={query.type} 
              onSave={state => setQuery({ ...query, type: state })}
            />
            <FilterEnvironment 
              savedValue={query.environments} 
              onSave={state => setQuery({ ...query, environments: state })}
            />
            <FilterAlignment 
              savedValue={query.alignment} 
              onSave={state => setQuery({ ...query, alignment: state })}
            />
            <FilterSize 
              savedValue={query.size} 
              onSave={state => setQuery({ ...query, size: state })}
            />
          </div>
          <SortableTable 
            className={styles.table}
            boldColumns={['name']}
            onClickRow={id => setMonsterSheet(id)}
            columns={[
              { value: 'name', label: 'Name', format: 'text' },
              { value: 'type', label: 'Type', format: 'text' },
              { value: 'size', label: 'Size', format: 'text' },
              { value: 'cr', label: 'CR', format: 'number' },
            ]}
            rows={(availableMonsters as SimpleMonster[]).map(mon =>  ({
              id: mon.index,
              values: pick(mon, ['name', 'type', 'size', 'cr']) as Pick<SimpleMonster, 'name' | 'type' | 'size' | 'cr'>,
              actions: [
                <IconButton 
                  key={`add-${mon.index}`} 
                  icon='plus' 
                  className={styles.plus} 
                  onClick={e => { 
                    e.stopPropagation()
                    const existsIdx = encounterState.creatures.map(cr => cr.creature).indexOf(mon.index)
                    const newCreatures: ECreature[] = JSON.parse(JSON.stringify(encounterState.creatures))
                    if (existsIdx !== -1) newCreatures[existsIdx].count++
                    else {
                      newCreatures.push({
                        creature: mon.index, 
                        count: 1
                      })
                    }
                    setEncounterState({ ...encounterState, creatures: newCreatures })
                  }} 
                />
              ]
            }))}
          />
        </div>
      </div>
      <div className={styles.summaryWrap}>
        <EncounterSummary 
          creatureMap={getCreatureMap(encounterState.creatures.map(cr => cr.creature))}
          encounter={encounterState} 
          onChange={newState => setEncounterState(newState)}
          onSubmit={() => {
            const {name, creatures, config} = encounterState
            createEncounter(name, creatures, config, targetSection)
            goToEncountersPage()
          }}
        />
      </div>
    </div>
  )

}

export default CreateEncounter