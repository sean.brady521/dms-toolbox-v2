import React, { useState } from 'react'
import { GenerateOptions, INpc, NpcSection } from 'types/npcs'
import styles from './npcs.module.scss'

import NpcCard from './components/npc-card'
import SectionedGrid from 'components/sectioned-grid'
import arrayMove from 'array-move'
import { generateNewNpc } from 'utils/npc-generator/npc-generator'
import { ISection } from 'types/common'
import { addBlankSection, getRootSection, reorderSections } from 'utils/sections'
import Dialog from 'components/dialog'
import MenuButton from 'components/menu-button'
import ConfigModal from './components/config-modal/config-modal'

export interface NpcsProps {
  npcs: NpcSection[]
  sections: ISection[]
  races?: string[]
  classes?: string[]
  professionsBySocial?: Map<string, string[]>
  addNpc: (npc: INpc, section: ISection) => void
  removeNpc: (id: string, section: ISection) => void
  addOrEditSections: (sections: ISection[]) => void
  removeSection: (id: string) => void
  redirectToNpc: (npcId: string) => void
}


const Npcs = (props: NpcsProps): JSX.Element => {
  const { 
    npcs,
    sections,
    races,
    classes,
    professionsBySocial,
    addNpc,
    addOrEditSections,
    removeSection,
    removeNpc,
    redirectToNpc
  } = props

  const [confirmDelete, setConfirmDelete] = useState<{ id: string, sec: ISection } | null>(null)
  const [highlightCard, setHighlightCard] = useState<string | null>('')
  const [showConfig, setShowConfig] = useState<boolean>(false)

  const cardsBySection = []
  for (const sec of npcs) {
    cardsBySection.push({
      id: sec.id,
      label: sec.label,
      showPlaceholder: sec.showPlaceholder,
      collapsed: sec.collapsed,
      items: sec.npcs.map(npc => (
        <NpcCard 
          onClick={() => { 
            redirectToNpc(npc.id)
            setHighlightCard(null)
          }}
          onDelete={() => { 
            const section = sections.find(s => s.id === sec.id)
            if (!section) return
            setConfirmDelete({ id: npc.id, sec: section })
          }}
          key={npc.id}
          highlight={npc.id === highlightCard}
          name={npc.name} 
          race={npc.race} 
          classOrProf={npc.class} 
          gender={npc.gender} 
        />
      ))
    })
  }

  const onChange = (oldSectionIdx: number, newSectionIdx: number, oldIndex: number, newIndex: number) => {
    if (oldSectionIdx !== newSectionIdx) { // moving groups
      const oldSec = sections[oldSectionIdx]
      const newSec = sections[newSectionIdx]

      const npcId = oldSec.itemIds[oldIndex]

      const newOldSec = {
        ...oldSec,
        itemIds: oldSec.itemIds.filter(id => id !== npcId)
      }

      const newNewSec = {
        ...newSec,
        itemIds: [...newSec.itemIds, npcId]
      }

      addOrEditSections([newOldSec, newNewSec])
    } else { // Same section
      const currSec = sections[oldSectionIdx]
      const newSec = {
        ...currSec,
        itemIds: arrayMove(currSec.itemIds, oldIndex, newIndex)
      }
      addOrEditSections([newSec])
    }
  }

  const generateNpc = (ops?: GenerateOptions) => {
    const npc = generateNewNpc(ops)
    const rootSection = getRootSection('npcs', sections)
    addNpc(npc, rootSection)
    setHighlightCard(npc.id)
  }

  return (
    <div className={styles.root}>
      <Dialog 
        open={!!confirmDelete}
        message='Are you sure you want to delete this NPC?'
        primaryText='Confirm'
        onPrimary={() => { 
          if (confirmDelete) {
            removeNpc(confirmDelete.id, confirmDelete.sec)
            setConfirmDelete(null)
          }
        }}
        secondaryText='Cancel'
        onSecondary={() => setConfirmDelete(null)}
        onClose={() => setConfirmDelete(null)}
      />
      <ConfigModal
        open={showConfig} 
        onClose={() => setShowConfig(false)}
        races={races ?? []}
        classes={classes ?? []}
        professionsBySocial={professionsBySocial ?? new Map<string, string[]>()}
        onSubmit={(ops: GenerateOptions) => {
          generateNpc(ops)
        }}
      />
      <MenuButton 
        className={styles.generateButton} 
        buttonText='Create'
        buttonIcon='plus circle'
        position='bottom left'
        options={[
          { 
            value: 'random', 
            label: 'Fully random', 
            subLabel: 'Generate a completely random NPC',
            group: 'Generate NPC',
            icon: 'magic' 
          },
          { 
            value: 'custom', 
            label: 'Custom', 
            subLabel: 'Choose the parameters of the generator',
            group: 'Generate NPC',
            icon: 'lab' 
          },
          { 
            value: 'section', 
            label: 'Create collection', 
            subLabel: 'Create a collection to group NPCs',
            group: 'Get organised',
            icon: 'object group' 
          },
        ]}
        onSelect={val => {
          switch(val) {
          case 'random':
            generateNpc()
            break
          case 'custom':
            setShowConfig(true)
            break
          case 'section':
            addOrEditSections(addBlankSection('npcs', sections))
            break
          }
        }}
      />
      <div className={styles.gridWrap}>
        <SectionedGrid 
          itemName='NPC'
          sections={cardsBySection} 
          onChange={onChange} 
          onChangeLabel={(secIdx: number, label: string) => {
            addOrEditSections([{ ...sections[secIdx], label }])
          }}
          onMoveSection={(oldIdx: number, newIdx: number) => {
            addOrEditSections(reorderSections(sections, oldIdx, newIdx) )
          }}
          onDeleteSection={(id: string) => {
            removeSection(id)
          }}
          onAddToSection={(id: string) => {
            const section = sections.find(sec => sec.id === id)
            if (!section) return

            const npc = generateNewNpc()
            addNpc(npc, section)
          }}
          onToggleCollapseSection={(id: string) => {
            const section = sections.find(sec => sec.id === id)
            if (!section) return

            addOrEditSections([{...section, collapsed: !section.collapsed}])
          }}
        />
      </div>
    </div>
  )
}

export default Npcs