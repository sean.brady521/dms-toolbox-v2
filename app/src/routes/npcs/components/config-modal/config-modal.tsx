import ButtonGroupSelect from 'components/button-group-select'
import React, { useState } from 'react'
import Select from 'components/select'
import Button from 'components/button'
import { Icon, Modal } from 'semantic-ui-react'
import { Option } from 'types/common'
import { GenerateOptions } from 'types/npcs'
import styles from './config-modal.module.scss'

export interface ConfigModalProps {
  open: boolean
  races: string[]
  classes: string[]
  professionsBySocial: Map<string, string[]>
  onClose: () => void
  onSubmit: (ops: GenerateOptions) => void
}

const RANDOM = { value: 'random', label: 'Random' }

const genderConf = [
  RANDOM,
  { value: 'male', label: 'Male' },
  { value: 'female', label: 'Female' }
]

const plotConf = [
  RANDOM,
  { value: 'classic', label: 'Classic' },
  { value: 'funky', label: 'Funky' }
]

const classOrProfConf = [
  RANDOM,
  { value: 'class', label: 'Class' },
  { value: 'prof', label: 'Prof' }
]

function getValueKey (arr: string[], val: string) {
  return ['random', ...arr].indexOf(val) - 1
}

function mapToGroupedOptions(map: Map<string, string[]>): Array<{ label: string, options: Option[] }> {
  const groupedOptions: Array<{ label: string, options: Option[] }> = []
  for (const key of Array.from(map.keys())) {
    groupedOptions.push({
      label: key,
      options: map.get(key)?.map(label => ({ value: `${key}|${label}`, label })) || []
    })
  }
  return groupedOptions
}


const ConfigModal = (props: ConfigModalProps): JSX.Element => {
  const { open, races, classes, professionsBySocial, onClose, onSubmit } = props

  const [gender, setGender] = useState<string>(RANDOM.value)
  const [plotHook, setPlotHook] = useState<string>(RANDOM.value)
  const [race, setRace] = useState<string>(RANDOM.value)
  const [classOrProf, setClassOrProf] = useState<string>(RANDOM.value)
  const [prof, setProf] = useState<string>(RANDOM.value)
  const [social, setSocial] = useState<string>(RANDOM.value)
  const [aClass, setAclass] = useState<string>(RANDOM.value)

  const handleGenerate = () => {
    let occupation1, occupation2
    if (classOrProf === 'class') {
      occupation1 = getValueKey(classes || [], aClass)
    } else if (classOrProf === 'prof') {
      occupation1 = getValueKey(Array.from(professionsBySocial.keys()), social)
      occupation2 = getValueKey(professionsBySocial.get(social) || [], prof)
    }

    const ops: GenerateOptions = {
      gender: getValueKey(['male', 'female'], gender),
      plothook: getValueKey(['classic', 'funky'], plotHook),
      classorprof: getValueKey(['class', 'prof'], classOrProf),
      occupation1,
      occupation2,
      race: getValueKey(races, race)
    }

    onSubmit(ops)
    onClose()
  }

  return (
    <Modal 
      open={open} 
      closeIcon={<Icon name='close' />}
      className={styles.modal} 
      size='tiny'
      onClose={onClose} 
    >
      <div className={styles.inner}>
        <h2 className={styles.title}>Custom NPC Generator</h2>
        <div className={styles.sectionLabel}>Gender</div>
        <ButtonGroupSelect 
          buttons={genderConf}
          selected={gender}
          onClick={val => setGender(val)}
        />
        <div className={styles.sectionLabel}>Plot hook</div>
        <ButtonGroupSelect 
          buttons={plotConf}
          selected={plotHook}
          onClick={val => setPlotHook(val)}
        />
        <div className={styles.sectionLabel}>Class or Profession</div>
        <ButtonGroupSelect 
          buttons={classOrProfConf}
          selected={classOrProf}
          onClick={val => setClassOrProf(val)}
        />
        {classOrProf === 'class' &&
          <Select 
            value={{ value: aClass, label: aClass === 'random' ? 'Random class' : aClass }}
            onChange={(e) => e && setAclass(e.value)}
            className={styles.select}
            options={[
              {value: 'random', label: 'Random class'},
              ...classes.map(value => ({ value, label: value }))
            ]}
          />
        }
        {classOrProf === 'prof' &&
          <Select 
            value={{ value: `${social}|${prof}`, label: prof === 'random' ? 'Random prof' : prof }}
            onChange={(e) => { 
              if (e) {
                const [social, prof] = e.value.split('|')
                setProf(prof)
                setSocial(social)
              }
            }}
            className={styles.select}
            options={[
              { value: 'random', label: 'Random profession' },
              ...mapToGroupedOptions(professionsBySocial)
            ]}
          />
        }
        <div className={styles.sectionLabel}>Race</div>
        <Select 
          value={{ value: race, label: race === 'random' ? 'Random race' : race }}
          onChange={(e) => e && setRace(e.value)}
          className={styles.raceSelect}
          options={[
            { value: 'random', label: 'Random race' },
            ...races.map(value => ({ value, label: value }))
          ]}
        />

        <div className={styles.submitWrap}>
          <Button className={styles.submit} onClick={handleGenerate}>
            Generate
          </Button>
        </div>
      </div>
    </Modal>
  )
}

export default ConfigModal