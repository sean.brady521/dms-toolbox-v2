import React from 'react'
import styles from './npc-card.module.scss'
import classNames from 'classnames'
import { Icon } from 'semantic-ui-react'

import Elf from 'assets/images/elf.png'
import Aasamir from 'assets/images/aasamir.png'
import Dragonborn from 'assets/images/dragonborn.png'
import DwarfEmoji from 'assets/images/dwarfEmoji.png'
import Firbolg from 'assets/images/firbolg.png'
import Gnome from 'assets/images/gnome.png'
import Goblin from 'assets/images/goblin.png'
import Goliath from 'assets/images/goliath.png'
import Halfling from 'assets/images/halfling.png'
import Human from 'assets/images/human.png'
import Kenku from 'assets/images/kenku.png'
import Lizardfolk from 'assets/images/lizardfolk.png'
import Medusa from 'assets/images/medusa.png'
import Orc from 'assets/images/orc.png'
import Tabaxi from 'assets/images/tabaxi.png'
import Tiefling from 'assets/images/tiefling.png'
import Triton from 'assets/images/triton.png'
import Troglodyte from 'assets/images/troglodyte.png'

const raceToImg: Record<string, string> = {
  aasimar: Aasamir,
  dragonborn: Dragonborn,
  dwarf: DwarfEmoji,
  firbolg: Firbolg,
  gnome: Gnome,
  goblin: Goblin,
  goliath: Goliath,
  halfling: Halfling,
  human: Human,
  kenku: Kenku,
  lizardfolk: Lizardfolk,
  medusa: Medusa,
  tabaxi: Tabaxi,
  tiefling: Tiefling,
  triton: Triton,
  troglodyte: Troglodyte,
  orc: Orc,
  elf: Elf,
}

export interface NpcCardProps {
  name?: string
  race?: string
  classOrProf?: string
  gender?: string
  className?: string
  highlight?: boolean
  onClick?: () => void
  onDelete?: () => void
}

const NpcCard = (props: NpcCardProps): JSX.Element => {
  const { name, race, classOrProf, gender, highlight, className, onClick, onDelete } = props
  const desc = `${gender} ${race} ${classOrProf}`

  let image
  if (race) {
    for (const [raceMatch, icon] of Object.entries(raceToImg)) {
      if (race.includes(raceMatch)) image = icon
    }
  }

  return (
    <div 
      className={classNames(styles.root, className, onClick && styles.clickable, highlight && styles.highlight)}
      onClick={onClick}
    >
      <Icon 
        name='trash alternate' 
        circular 
        className={styles.delete} 
        onClick={(e: React.MouseEvent) => {
          e.stopPropagation()
          onDelete && onDelete()
        }} 
      />
      <div className={styles.imageAndName}>
        <img src={image} className={styles.image} />
        <div className={styles.name}>{name}</div>
      </div>
      <div className={styles.desc}>
        {desc}
      </div>
    </div>
  )
}

export default NpcCard