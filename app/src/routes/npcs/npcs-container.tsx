import React from 'react'
import { connect } from 'react-redux'

import { getNamedTableOptions, getTableReferenceOptions, TableReferenceOption } from 'utils/npc-generator/npc-data/tables'

import Npcs, { NpcsProps } from './npcs'
import { ISection, StoreState } from 'types/common'
import { selectNpcsBySection } from 'store/npcs/npcs-selectors'
import { Dispatch } from 'redux'
import { INpc } from 'types/npcs'
import { postNpc, removeNpc } from 'store/npcs/npcs-actions'
import { selectOrderedSections, selectScopedSections } from 'store/sections/sections-selectors'
import { addRecToSection, postSections, removeRecFromSection, removeSection } from 'store/sections/sections-actions'
import { replace } from 'connected-react-router'

export const NpcsContainer = (props: NpcsProps): JSX.Element => {
  return <Npcs {...props} />
}

export function mapStateToProps(state: StoreState): Pick<NpcsProps, 'npcs' | 'races' | 'classes' | 'professionsBySocial' | 'sections'> {
  const { npcs, sections } = state
  const races: string[] = getTableReferenceOptions('race').map(r => r.name || '')
  const classes = getNamedTableOptions('class').map(c => c.name || '')

  const npcSections = selectOrderedSections(selectScopedSections(sections.sections, 'npcs'))
  
  const professionsBySocial: Map<string, string[]> = new Map()
  const socialClassTableRefs = getTableReferenceOptions('profession')
  for (const classOb of socialClassTableRefs) {
    professionsBySocial.set(classOb.name || '', [])
    const profs = getNamedTableOptions(classOb.table).map(c => c.name || '')
    professionsBySocial.set(classOb.name || '', profs)
  }

  return {
    races,
    classes,
    professionsBySocial,
    npcs: selectNpcsBySection(npcs.npcs, npcSections),
    sections: npcSections,
  }
}

type NpcDispatchProps = 'addNpc' | 'addOrEditSections' | 'removeSection' | 'removeNpc' | 'redirectToNpc'
export function mapDispatchToProps(dispatch: Dispatch): Pick<NpcsProps, NpcDispatchProps> {
  return {
    addNpc: (npc: INpc, section: ISection) => {
      postNpc(npc)(dispatch)
      addRecToSection(npc.id, section)(dispatch)
    },
    addOrEditSections: (sections: ISection[]) => {
      if (sections.length === 0) return
      postSections(sections)(dispatch)
    },
    removeSection: (id: string) => {
      removeSection(id)(dispatch)
    },
    removeNpc: (id: string, section: ISection) => {
      removeNpc(id)(dispatch)
      removeRecFromSection(id, section)(dispatch)
    },
    redirectToNpc: (npcId: string): void => {
      dispatch(replace(`/npc/${npcId}`))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NpcsContainer)