import React from 'react'
import Button from 'components/button'
import styles from './teaser.module.scss'

export interface TeaserProps {
  image: string
  imageSide?: string
  primaryText: string
  secondaryText: string
  buttonText: string
  onClick: () => void
}


const Teaser = (props: TeaserProps): JSX.Element => {
  const { image, imageSide = 'right', primaryText, secondaryText, buttonText, onClick } = props

  const TextSection = (
    <div className={styles.textWrap} key='text'>
      <div className={styles.primary}>
        {primaryText}
      </div>
      <div className={styles.secondary}>
        {secondaryText}
      </div>
      <Button onClick={onClick} secondary>
        {buttonText}
      </Button>
    </div>
  )

  const ImageSection = <img key='im' src={image} className={styles.image} />

  const sections = [TextSection, ImageSection]
  if (imageSide === 'left') {
    sections.reverse()
  }

  return (
    <div className={styles.root}>
      {sections}
    </div>
  )
}

export default Teaser