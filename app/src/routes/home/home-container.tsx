import React from 'react'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { replace } from 'connected-react-router'

import { StoreState, } from 'types/common'

import Home, { HomeProps } from './home'

export const HomeContainer = (props: HomeProps): JSX.Element => {
  return <Home {...props} />
}

export function mapStateToProps(state: StoreState): Pick<HomeProps, 'loggedIn'> {
  const { loggedIn } = state.user
  return { loggedIn }
}

export function mapDispatchToProps(dispatch: Dispatch): Pick<HomeProps, 'changePage'> {
  return {
    changePage: (page: string): void => {
      dispatch(replace(page))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer)