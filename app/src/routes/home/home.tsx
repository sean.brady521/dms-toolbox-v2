import React from 'react'
import styles from './home.module.scss'

import dwarfImage from 'assets/images/dwarf.png'
import encounterImage from 'assets/images/encounter.png'
import sorcererImage from 'assets/images/sorcerer.png'
import Teaser from './components/teaser'

export interface HomeProps {
  loggedIn: boolean
  changePage: (page: string) => void
}

const teasers = [
  {
    primary: 'Randomly generate detailed NPCs on the fly',
    secondary: 'Generate someone completely random and potentially hilarious. Or take control and generate a character specific to your context',
    button: 'Generate NPC\'s',
    image: dwarfImage,
    page: 'npcs'
  },
  {
    primary: 'Generate balanced encounters with personality ',
    secondary: 'Randomly generate an encounter based on your parties strength and location, as well as the DM\'s desired difficulty. Encounters include monster relationships and personality.',
    button: 'Generate encounters',
    image: encounterImage,
    page: 'encounters',
    imageSide: 'left'
  },
  {
    primary: 'Sync your data across devices',
    secondary: 'Sign up securely with our passworldless login to sync your data automatically across devices',
    button: 'Create account',
    image: sorcererImage,
    page: 'signup'
  },

]


const Home = (props: HomeProps): JSX.Element => {
  const { changePage } = props
  return (
    <div className={styles.root}>
      <div className={styles.teaserWrap}>
        {teasers.map((t, idx) => (
          <div className={styles.teaser} key={idx}>
            <Teaser 
              key={`teaser-${idx}`}
              primaryText={t.primary} 
              secondaryText={t.secondary}
              imageSide={t.imageSide}
              buttonText={t.button}
              image={t.image}
              onClick={() => changePage(t.page)}
            /> 
          </div>
        ))}
      </div>
    </div>
  )
}

export default Home