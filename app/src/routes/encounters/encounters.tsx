import React, { useState } from 'react'
import { IEncounter, EncounterSection, EncounterConfig } from 'types/encounters'
import styles from './encounters.module.scss'

import EncounterCard from './components/encounter-card'
import SectionedGrid from 'components/sectioned-grid'
import arrayMove from 'array-move'
import { generateNewEncounter } from 'utils/encounter-generator/generator'
import { ISection } from 'types/common'
import { addBlankSection, getRootSection, reorderSections } from 'utils/sections'
import Dialog from 'components/dialog'
import MenuButton from 'components/menu-button'
import ConfigModal from './components/config-modal'
import { MonsterData } from 'types/monster'

export interface EncountersProps {
  encounters: EncounterSection[]
  creatures: Record<string, MonsterData>
  loadingCreatures: boolean
  sections: ISection[]
  addEncounter: (encounter: IEncounter, section: ISection) => void
  removeEncounter: (id: string, section: ISection) => void
  addOrEditSections: (sections: ISection[]) => void
  removeSection: (id: string) => void
  goToEncounterPage: (encounterId: string) => void
  goToCreateEncounterPage: () => void
  fetchMonsters: (creatures: string[]) => void
}


const Encounters = (props: EncountersProps): JSX.Element => {
  const { 
    encounters,
    creatures,
    loadingCreatures,
    sections,
    addEncounter,
    addOrEditSections,
    removeSection,
    removeEncounter,
    goToEncounterPage,
    goToCreateEncounterPage,
    fetchMonsters
  } = props

  const [confirmDelete, setConfirmDelete] = useState<{ id: string, sec: ISection } | null>(null)
  const [showConfig, setShowConfig] = useState<boolean>(false)
  const [highlightCard, setHighlightCard] = useState<string | null>('')

  const cardsBySection = []
  for (const sec of encounters) {
    cardsBySection.push({
      id: sec.id,
      label: sec.label,
      showPlaceholder: sec.showPlaceholder,
      collapsed: sec.collapsed,
      items: sec.encounters.map(encounter => (
        <EncounterCard 
          creatureData={creatures}
          loadingCreatures={loadingCreatures}
          onClick={() => { 
            goToEncounterPage(encounter.id)
            setHighlightCard(null)
          }}
          onDelete={() => { 
            const section = sections.find(s => s.id === sec.id)
            if (!section) return
            setConfirmDelete({ id: encounter.id, sec: section })
          }}
          key={encounter.id}
          highlight={encounter.id === highlightCard}
          fetchMonsters={fetchMonsters}
          name={encounter.name} 
          creatures={encounter.creatures}
        />
      ))
    })
  }

  const onChange = (oldSectionIdx: number, newSectionIdx: number, oldIndex: number, newIndex: number) => {
    if (oldSectionIdx !== newSectionIdx) { // moving groups
      const oldSec = sections[oldSectionIdx]
      const newSec = sections[newSectionIdx]

      const encounterId = oldSec.itemIds[oldIndex]

      const newOldSec = {
        ...oldSec,
        itemIds: oldSec.itemIds.filter(id => id !== encounterId)
      }

      const newNewSec = {
        ...newSec,
        itemIds: [...newSec.itemIds, encounterId]
      }

      addOrEditSections([newOldSec, newNewSec])
    } else { // Same section
      const currSec = sections[oldSectionIdx]
      const newSec = {
        ...currSec,
        itemIds: arrayMove(currSec.itemIds, oldIndex, newIndex)
      }
      addOrEditSections([newSec])
    }
  }

  const generateEncounter = (conf: EncounterConfig) => {
    const encounter = generateNewEncounter(conf)
    if (!encounter) return
    const rootSection = getRootSection('encounters', sections)
    addEncounter(encounter, rootSection)
    setHighlightCard(encounter.id)
  }

  return (
    <div className={styles.root}>
      <Dialog 
        open={!!confirmDelete}
        message='Are you sure you want to delete this encounter?'
        primaryText='Confirm'
        onPrimary={() => { 
          if (confirmDelete) {
            removeEncounter(confirmDelete.id, confirmDelete.sec)
            setConfirmDelete(null)
          }
        }}
        secondaryText='Cancel'
        onSecondary={() => setConfirmDelete(null)}
        onClose={() => setConfirmDelete(null)}
      />
      <ConfigModal
        open={showConfig} 
        onClose={() => setShowConfig(false)}
        onSubmit={(ops: EncounterConfig) => {
          generateEncounter(ops)
        }}
      />
      <MenuButton 
        className={styles.generateButton} 
        buttonText='Create'
        buttonIcon='plus circle'
        position='bottom left'
        options={[
          { 
            value: 'random', 
            label: 'Randomly generate', 
            subLabel: 'Completely random encounter',
            group: 'Create encounter',
            icon: 'magic' 
          },
          { 
            value: 'create', 
            label: 'Create from scratch', 
            subLabel: 'Build your encounter from the ground up',
            group: 'Create encounter',
            icon: 'lab' 
          },
          { 
            value: 'section', 
            label: 'Create collection', 
            subLabel: 'Create a collection to group encounters',
            group: 'Get organised',
            icon: 'object group' 
          },
        ]}
        onSelect={val => {
          switch(val) {
          case 'random':
            setShowConfig(true)
            break
          case 'create':
            goToCreateEncounterPage()
            break
          case 'section':
            addOrEditSections(addBlankSection('encounters', sections))
            break
          }
        }}
      />
      <div className={styles.gridWrap}>
        <SectionedGrid 
          itemName='encounter'
          sections={cardsBySection} 
          onChange={onChange} 
          onChangeLabel={(secIdx: number, label: string) => {
            addOrEditSections([{ ...sections[secIdx], label }])
          }}
          onMoveSection={(oldIdx: number, newIdx: number) => {
            addOrEditSections(reorderSections(sections, oldIdx, newIdx) )
          }}
          onDeleteSection={(id: string) => {
            removeSection(id)
          }}
          onToggleCollapseSection={(id: string) => {
            const section = sections.find(sec => sec.id === id)
            if (!section) return

            addOrEditSections([{...section, collapsed: !section.collapsed}])
          }}
        />
      </div>
    </div>
  )
}

export default Encounters