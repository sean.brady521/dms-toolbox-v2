import React from 'react'
import { connect } from 'react-redux'

import Encounters, { EncountersProps } from './encounters'
import { ISection, StoreState } from 'types/common'
import { selectEncountersBySection } from 'store/encounters/encounters-selectors'
import { Dispatch } from 'redux'
import { IEncounter } from 'types/encounters'
import { fetchCreatures, postEncounter, removeEncounter } from 'store/encounters/encounters-actions'
import { selectOrderedSections, selectScopedSections } from 'store/sections/sections-selectors'
import { addRecToSection, postSections, removeRecFromSection, removeSection } from 'store/sections/sections-actions'
import { replace } from 'connected-react-router'

export const EncountersContainer = (props: EncountersProps): JSX.Element => {
  return <Encounters {...props} />
}

export function mapStateToProps(state: StoreState): Pick<EncountersProps, 'encounters' | 'sections' | 'creatures' | 'loadingCreatures'> {
  const { encounters, sections } = state

  const encounterSections = selectOrderedSections(selectScopedSections(sections.sections, 'encounters'))
  
  return {
    encounters: selectEncountersBySection(encounters.encounters, encounterSections),
    sections: encounterSections,
    creatures: encounters.creatureCache,
    loadingCreatures: encounters.fetchingCreatures
  }
}

type EncounterDispatchProps = 'addEncounter' | 'addOrEditSections' | 'removeSection' | 'removeEncounter' | 'goToEncounterPage' | 'goToCreateEncounterPage' | 'fetchMonsters'
export function mapDispatchToProps(dispatch: Dispatch): Pick<EncountersProps, EncounterDispatchProps> {
  return {
    addEncounter: (encounter: IEncounter, section: ISection) => {
      postEncounter(encounter)(dispatch)
      addRecToSection(encounter.id, section)(dispatch)
    },
    addOrEditSections: (sections: ISection[]) => {
      if (sections.length === 0) return
      postSections(sections)(dispatch)
    },
    removeSection: (id: string) => {
      removeSection(id)(dispatch)
    },
    removeEncounter: (id: string, section: ISection) => {
      removeEncounter(id)(dispatch)
      removeRecFromSection(id, section)(dispatch)
    },
    goToEncounterPage: (encounterId: string): void => {
      dispatch(replace(`/encounter/${encounterId}`))
    },
    goToCreateEncounterPage: () => {
      dispatch(replace('/create-encounter'))
    },
    fetchMonsters: (creatures: string[]) => {
      fetchCreatures(creatures)(dispatch)
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EncountersContainer)