import React, { useState } from 'react'
import Select from 'components/select'
import Button from 'components/button'
import { Icon, Modal } from 'semantic-ui-react'
import styles from './config-modal.module.scss'
import { EncounterConfig } from 'types/encounters'
import { Difficulty, Environment } from 'constants/enums'
import NumberStepper from 'components/number-stepper'
import SkullImage from 'assets/images/skull.png'
import Slider from 'components/slider'

export interface ConfigModalProps {
  open: boolean
  onClose: () => void
  onSubmit: (ops: EncounterConfig) => void
}


const ConfigModal = (props: ConfigModalProps): JSX.Element => {
  const { open, onClose, onSubmit } = props

  const [avgLvl, setAvgLvl] = useState<number>(3)
  const [numPcs, setNumPcs] = useState<number>(4)
  const [environment, setEnvironment] = useState<Environment>(Environment.Forest)
  const [difficulty, setDifficulty] = useState<Difficulty>(Difficulty.Easy)

  const skulls = []
  for (let i = 0 ; i < difficulty; i++) {
    skulls.push(<img key={`skull-${i}`} src={SkullImage} className={styles.skull} />)
  }

  const handleGenerate = () => {
    const conf: EncounterConfig = {
      avgLvl,
      numPcs,
      environment,
      difficulty
    }
    onSubmit(conf)
    onClose()
  }

  return (
    <Modal 
      open={open} 
      closeIcon={<Icon name='close' />}
      className={styles.modal} 
      size='tiny'
      onClose={onClose} 
    >
      <div className={styles.inner}>
        <h2 className={styles.title}>Random Encounter Generator</h2>
        <div className={styles.sectionLabel}>Average player level</div>
        <NumberStepper 
          value={avgLvl} 
          className={styles.stepper}
          min={1}
          max={20}
          onChange={val => setAvgLvl(val)}
        />
        <div className={styles.sectionLabel}>Number of players</div>
        <NumberStepper 
          value={numPcs} 
          min={1}
          max={8}
          className={styles.stepper}
          onChange={val => setNumPcs(val)}
        />
        <div className={styles.sectionLabel}>Environment</div>
        <Select 
          value={environment ? { value: environment, label: environment } : undefined}
          onChange={(e) => e && setEnvironment(e.value as Environment)}
          className={styles.select}
          options={Object.values(Environment).map(env => ({ label: env, value: env }))}
        />
        <div className={styles.sectionLabel}>Difficulty</div>
        <Slider 
          className={styles.slider}
          value={difficulty}
          min={Difficulty.Easy}
          max={Difficulty.Deadly}
          onChange={val => setDifficulty(val as Difficulty)}
        />
        <div className={styles.difficultyWrap}>
          <div className={styles.difficultyText}>
            {Difficulty[difficulty]}
          </div>
          <div className={styles.skulls}>
            {skulls}
          </div>
        </div>
        <div className={styles.submitWrap}>
          <Button className={styles.submit} onClick={handleGenerate}>
            Generate
          </Button>
        </div>
      </div>
    </Modal>
  )
}

export default ConfigModal