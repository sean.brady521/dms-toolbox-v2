import React, { useEffect, useState } from 'react'
import styles from './encounter-card.module.scss'
import classNames from 'classnames'
import { Icon } from 'semantic-ui-react'
import { ECreature } from 'types/encounters'
import MonsterSheet from 'components/monster-sheet'
import { MonsterData } from 'types/monster'

export interface EncounterCardProps {
  name: string
  creatures: ECreature[]
  className?: string
  highlight?: boolean
  creatureData: Record<string, MonsterData>
  loadingCreatures: boolean
  onClick?: () => void
  onDelete?: () => void
  fetchMonsters: (creatures: string[]) => void
}

const EncounterCard = (props: EncounterCardProps): JSX.Element => {
  const { name, creatures, creatureData, loadingCreatures, highlight, className, onClick, onDelete, fetchMonsters } = props

  const [monsterSheet, setMonsterSheet] = useState<string | null>(null)

  useEffect(() => {
    if (!monsterSheet || creatureData[monsterSheet]) return
    fetchMonsters([monsterSheet])
  }, [monsterSheet])

  const creatureCount = creatures.reduce((acc, v) => acc + v.count, 0)

  return (
    <div 
      className={classNames(styles.root, className, onClick && styles.clickable, highlight && styles.highlight)}
      onClick={onClick}
    >
      <MonsterSheet 
        open={monsterSheet !== null} 
        onClose={() => { 
          setMonsterSheet(null)
        }}
        loading={loadingCreatures}
        monster={monsterSheet ? creatureData[monsterSheet] ?? null : null}
      />
      <Icon 
        name='trash alternate' 
        circular 
        className={styles.delete} 
        onClick={(e: React.MouseEvent) => {
          e.stopPropagation()
          onDelete && onDelete()
        }} 
      />
      <h3 className={styles.name}>
        {name}
      </h3>
      <div className={styles.count}>
        {`${creatureCount} monsters`}
      </div>
      <div className={styles.chips}>
        {creatures.map(mon => (
          <div 
            className={styles.chipWrap} 
            key={mon.creature} 
            onClick={e => { 
              e.stopPropagation()
              setMonsterSheet(mon.creature)
            }}
          >
            {mon.count > 1 &&
              <div className={styles.badge}>
                {mon.count}
              </div>
            }
            <div className={styles.chip} >
              {mon.creature}
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default EncounterCard