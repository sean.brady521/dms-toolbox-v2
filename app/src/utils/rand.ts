// this function is INCLUSIVE [from, to]
export function randBetween (from: number, to: number): number {
  return Math.floor(Math.random() * (to - from + 1) + from)
}


export function randSelect<T> (arr: T[]): T {
  return arr[randBetween(0, arr.length - 1)]
}