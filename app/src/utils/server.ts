import { MonsterData } from 'types/monster'
import getEndpointBase from '../config/endpoint'
import { getIdToken } from './auth'

export type Pojo = Record<string, unknown> | Record<string, unknown>[]
type FailureFunc = (err: Error) => void
type SuccessFunc = (data?: Pojo) => void

// Handle HTTP errors since fetch won't.
function _handleErrors(response: Response) {
  if (!response.ok) {
    throw Error(`${response.status}: ${response.statusText}`)
  }
  return response
}

async function _fetch(url: string, onSuccess: SuccessFunc, onFailure: FailureFunc, options: RequestInit = {}, expectResponse = true): Promise<void> {
  return fetch(url, options)
    .then(_handleErrors)
    .then(res => expectResponse ? res.json() : onSuccess())
    .then(json => { if (expectResponse) onSuccess(json) })
    .catch(err => { 
      onFailure(err.message)
    })
}

async function _get(url: string, onSuccess: SuccessFunc, onFailure: FailureFunc, withAuth = false): Promise<void> {
  const options: RequestInit = {}
  if (withAuth) {
    const token = await getIdToken()
    options.headers = {
      'Authorization': token
    } as HeadersInit
  }
  return _fetch(url, onSuccess, onFailure, options)
}

async function _post(url: string, body: Pojo, onSuccess: SuccessFunc, onFailure: FailureFunc, expectResponse = false): Promise<void> {
  const token = await getIdToken()
  const options: RequestInit = {
    headers: { 
      'Content-Type': 'application/json',
      'Authorization': token
    } as HeadersInit,
    method: 'POST',
    body: JSON.stringify(body)
  }
  return _fetch(url, onSuccess, onFailure, options, expectResponse)
}

async function _delete(url: string, body: Pojo, onSuccess: SuccessFunc, onFailure: FailureFunc): Promise<void> {
  const token = await getIdToken()
  const options: RequestInit = {
    headers: { 
      'Content-Type': 'application/json',
      'Authorization': token
    } as HeadersInit,
    method: 'DELETE',
    body: JSON.stringify(body)
  }
  return _fetch(url, onSuccess, onFailure, options)
}

export async function addOrModifyEntry (endpoint: string, entry: Pojo, onSuccess: SuccessFunc, onFailure: FailureFunc): Promise<void> {
  const url =  `${getEndpointBase()}/api/${endpoint}`
  return _post(url, entry, onSuccess, onFailure)
}

export async function removeById (endpoint: string, id: string, onSuccess: SuccessFunc, onFailure: FailureFunc): Promise<void> {
  const url =  `${getEndpointBase()}/api/${endpoint}`
  return _delete(url, { id }, onSuccess, onFailure)
}

export async function fetchAll (endpoint: string, onSuccess: SuccessFunc, onFailure: FailureFunc): Promise<void> {
  const url = `${getEndpointBase()}/api/${endpoint}`
  return _get(url, onSuccess, onFailure, true)
}

export async function fetchById (endpoint: string, id: string, onSuccess: SuccessFunc, onFailure: FailureFunc): Promise<void> {
  const url = `${getEndpointBase()}/api/${endpoint}/${id}`
  return _get(url, onSuccess, onFailure, true)
}

export async function fetch5eMonsters (creatures: string[], onSuccess: SuccessFunc, onFailure: FailureFunc): Promise<void> {
  const requests = []
  const results: Pojo = {}
  const collector = (data?: Pojo) => results[(data as unknown as MonsterData).index] = data
  for (const mon of creatures) {
    requests.push(_fetch(`https://www.dnd5eapi.co/api/monsters/${mon.replace(' ', '-')}`, collector, onFailure))
  }
  await Promise.all(requests)
  onSuccess(results)
}

