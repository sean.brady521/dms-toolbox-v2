import { Difficulty, Environment } from 'constants/enums'
// import monsters from './monsters.json'
import { randSelect } from 'utils/rand'
import { thresholds, personalities, relationships } from './constants'
import { EncounterConfig, IEncounter, Personality, Relationship, ECreature } from 'types/encounters'
import { SimpleMonster } from 'types/monster'
import { v4 as uuidv4 } from 'uuid'
import { getUserId } from 'utils/auth'
import monsters from './monsters.json'
import encounterSample from '../encounter-sample.json'

export function personalityToString (pers: Personality): string {
  return `${pers.type}; ${pers.description}`
}

export function relationshipToString (rel: Relationship, monster?: string): string {
  return `One ${monster || 'enemy'} is ${rel.joiner}${rel.type}, ${rel.description}`
}

function determineXp (numPcs: number, avgLvl: number, difficulty: Difficulty): number {
  return thresholds[(avgLvl - 1)][(difficulty - 1)] * numPcs
}


function encounterGen (monsterList: SimpleMonster[], xpThreshold: number): SimpleMonster[] {
  const xpLowerLimit = Math.round(xpThreshold / 25)
  let xpMonsters = 0
  let monsterCount = 0
  const encounteredMonsters: SimpleMonster[] = []
  while (xpMonsters < (xpThreshold - (3 * xpLowerLimit))) {
    const possibleMonsters = []
    for (const mon of monsterList) {
      if (mon.xp > xpLowerLimit && mon.xp < (xpThreshold - xpMonsters)) {
        possibleMonsters.push(mon)
      }
    }
    if (!possibleMonsters.length) {
      console.warn('Ran out of suitable monsters')
      return encounteredMonsters
    }
    encounteredMonsters.push(randSelect(possibleMonsters))
    monsterCount = encounteredMonsters.length
    xpMonsters = 0
    for (const mon of encounteredMonsters) {
      xpMonsters += mon.xp
    }

    let countMultplier = 1
    if (monsterCount === 2) countMultplier = 1.5
    else if (monsterCount >= 3 && monsterCount <= 6)  countMultplier = 2
    else if (monsterCount >= 7 && monsterCount <= 10)  countMultplier = 2.5

    xpMonsters *= countMultplier
  }
  return encounteredMonsters
}

function filterMonsters (monsterList: SimpleMonster[], environment: Environment): SimpleMonster[] {
  return monsterList.filter(mon => mon.environments.includes(environment))
} 

interface RawGenerate {
  creatures: ECreature[]
  personality: Personality
  relationship: Relationship
  uniqueMonsters:  { [creature: string]: number }
}

export function _generate (numPcs: number, avgLvl: number, difficulty: Difficulty, environment: Environment): RawGenerate  {
  const xp = determineXp(numPcs, avgLvl, difficulty)
  const filteredMonsters = filterMonsters(monsters, environment)
  const encounteredMonsters = encounterGen(filteredMonsters, xp)

  const uniqueMonsters: { [creature: string]: number } = {}
  for (const mon of encounteredMonsters) {
    if (!uniqueMonsters[mon.index]) uniqueMonsters[mon.index] = 0
    uniqueMonsters[mon.index] += 1
  }
  const creatures = []
  for (const [creature, count] of Object.entries(uniqueMonsters)) {
    creatures.push({
      count,
      creature
    })
  }

  const personality = randSelect(personalities)
  const relationship = randSelect(relationships)

  return {
    creatures,
    personality,  
    relationship,
    uniqueMonsters
  }
}

export function getSampleEncounter (): IEncounter {
  return addNewEncounterMeta(encounterSample)
}

export function generateRandomEncounter (
  numPcs: number, 
  avgLvl: number, 
  difficulty: Difficulty, 
  environment: Environment
): Pick<IEncounter, 'creatures' | 'personality' | 'relationship' | 'name' | 'config'> {
  const { creatures, personality, relationship, uniqueMonsters } = _generate(numPcs, avgLvl, difficulty, environment)

  const totalMonsters = Object.values(uniqueMonsters).reduce((a,b) => a + b, 0)

  const personalityStr = personalityToString(personality)

  const randomMonster = randSelect(Object.keys(uniqueMonsters))
  const relationshipStr = totalMonsters > 1 
    ? relationshipToString(relationship, randomMonster)
    : 'No Relationship'

  return  {
    name: 'Random Encounter',
    creatures,
    personality: personalityStr,
    relationship: relationshipStr,
    config: { numPcs, avgLvl, difficulty, environment }
  }
}

export function addNewEncounterMeta(encounter: Pick<IEncounter, 'creatures' | 'personality' | 'relationship' | 'name' | 'config'>): IEncounter {
  const userId = getUserId()
  if (!userId) {
    throw new Error()
  } 

  const baseEncounter = {
    modifiedAt: Date.now(),
    createdAt: Date.now(),
    createdBy: userId,
    id: uuidv4(),
  }

  return {...baseEncounter, ...encounter}

}

export function generateNewEncounter (options: EncounterConfig): IEncounter | null {
  if (!options.difficulty || !options.environment) return null
  const randomEncounter = generateRandomEncounter(options.numPcs, options.avgLvl, options.difficulty, options.environment)
  return addNewEncounterMeta(randomEncounter)
}
