import { Personality, Relationship } from 'types/encounters'

export const BLANK_CR = -1

export const personalities: Personality[] = [
  {
    type: 'cowardly',
    description: 'looking	to	surrender'
  },
  {
    type: 'greedy',
    description: 'wants	treasure'
  },
  {
    type: 'braggart',
    description: 'makes	a	show	of	bravery	but	runs	if	in danger'
  },
  {
    type: 'fanatic',
    description: 'ready	to	die	fighting'
  },
  {
    type: 'rabble',
    description: 'poorly	trained	and	easily	rattled'
  },
  {
    type: 'brave',
    description: 'stands	its	ground'
  },
  {
    type: 'joker',
    description: 'taunts	enemies'
  },
  {
    type: 'bully',
    description: 'refuses	to	think	it	can	lose'
  }
]

export const relationships: Relationship[] = [
  {
    type: 'rival',
    joiner: 'a ',
    description: 'wants	one	random	ally	to	suffer'
  },
  {
    type: 'abused',
    joiner: '',
    description: 'hangs	back,	betrays	at	first	chance'
  },
  {
    type: 'worshipped',
    joiner: '',
    description: 'others	will	die	for	it'
  },
  {
    type: 'outcast',
    joiner: 'an ',
    description: 'others	ignore	it'
  },
  {
    type: 'mercenary',
    joiner: 'a ',
    description: 'cares	only	for	self'
  },
  {
    type: 'bully',
    joiner: 'a ',
    description: 'allies	want	to	see	it	defeated'
  }
]

export const thresholds = [
  [25, 50, 75, 100],
  [50, 100, 150, 200],
  [75, 150, 225, 400],
  [125, 250, 375, 500],
  [250, 500, 750, 1100],
  [300, 600, 900, 1400],
  [350, 750, 1100, 1700],
  [450, 900, 1400, 2100],
  [550, 1100, 1600, 2400],
  [600, 1200, 1900, 2800],
  [800, 1600, 2400, 3600],
  [1000, 2000, 3000, 4500],
  [1100, 2200, 3400, 5100],
  [1250, 2500, 3800, 5700],
  [1400, 2800, 4300, 6400],
  [1600, 3200, 4800, 7200],
  [2000, 3900, 5900, 8800],
  [2100, 4200, 6300, 9500],
  [2400, 4900, 7300, 10900],
  [2800, 5700, 8500, 12700]
]
