export function minMaxString(range?: { min?: string | number, max?: string | number }): string | null {
  if (!range) return null

  const { min, max } = range

  if (min === undefined && max === undefined) return null

  if (min !== undefined && max === undefined) {
    return `at least ${min}`
  }

  if (min === undefined && max !== undefined) {
    return `at most ${max}`
  }

  return `${min} - ${max}`
}