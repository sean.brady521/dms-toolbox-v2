import { paceOptions, pitchOptions, toneOptions, speechQuirkOptions, physicalQuirkOptions } from './npc-data/dnd-voices'
import { randSelect } from 'utils/rand'

export function generateVoice () {
  return {
    pace: randSelect(paceOptions),
    pitch: randSelect(pitchOptions),
    tone: randSelect(toneOptions),
    speechQuirk: randSelect(speechQuirkOptions),
    physicalQuirk: randSelect(physicalQuirkOptions)
  }
}
