import { generate } from './npc-data/generate'
import { generateVoice } from './voice-generator'
import { INpc, GenerateOptions } from 'types/npcs'
import { combineDescription, combinePersonalityTraits } from './paragraph-creator'
import { deepClone } from 'utils/clone'
import { v4 as uuidv4 } from 'uuid'
import { getUserId } from 'utils/auth'
import npcSample from '../npc-sample.json'


export interface AlignmentDetailed {
  chaotic: number
  ethicalneutral: number
  evil: number
  good: number
  lawful: number
  moralneutral: number
}

function getAlignmentStr (alignment: AlignmentDetailed): string {
  const firstPartAl = ['chaotic', 'ethicalneutral', 'lawful'] as (keyof AlignmentDetailed)[]
  const secondPartAl = ['good', 'evil', 'moralneutral'] as (keyof AlignmentDetailed)[]
  const firstPart = firstPartAl.sort((a, b) => parseInt(`${alignment[a]}`) - parseInt(`${alignment[b]}`))[0]
  const secondPart = secondPartAl.sort((a, b) => parseInt(`${alignment[a]}`) - parseInt(`${alignment[b]}`))[0]
  return `${firstPart} ${secondPart}`.replace('ethical', '').replace('moral', '')
}

export function extractTitle(npc: INpc): string {
  const rgx = /is a \d+ year old (\w+) (\w+) (\w+)\./
  const match = npc.description ? npc.description.match(rgx) || [] : []

  const gender = npc.gender || match[1]
  const race = npc.race || match[2]
  const classorprof = npc.class || match[3]

  let title = ''
  if (gender) title += gender + ' '
  if (race) title += race + ' '
  if (classorprof) title += classorprof

  return title 
}

export function generateRandomNpc (options: GenerateOptions): Partial<INpc> {
  const generated = generate(options).npc

  const alignment = getAlignmentStr(generated.alignment) 
  const name = generated.description.name
  const hook = generated.hook.description
  const description = combineDescription(generated)
  const personality = combinePersonalityTraits(generated)
  const gender = generated.description.gender
  const race = generated.description.race
  const occupationOrClass = generated.description.occupation
  const age = generated.description.age

  return {
    name,
    alignment,
    voice: generateVoice(),
    gender,
    age,
    class: occupationOrClass,
    race,
    description,
    personality,
    hook,
    abilities: generated.abilities
  }
}

export function getSampleNpc (): INpc {
  return {
    ..._getBaseNpc(),
    ...npcSample
  }
}

function _getBaseNpc (options: GenerateOptions = {}): Pick<INpc, 'modifiedAt' | 'createdAt' | 'createdBy' | 'id' | 'config'> {
  const userId = getUserId()
  if (!userId) {
    throw new Error()
  } 

  const baseNpc = {
    modifiedAt: Date.now(),
    createdAt: Date.now(),
    createdBy: userId,
    id: uuidv4(),
    config: options
  }

  return baseNpc

}

export function generateNewNpc (options: GenerateOptions = {}): INpc {
  return {..._getBaseNpc(options), ...generateRandomNpc(options)}
}

export function duplicateNpc (npc: INpc): INpc {
  const now = Date.now()
  const clonedNpc = deepClone<INpc>(npc)
  return {...clonedNpc, modifiedAt: now, createdAt: now, id: uuidv4()}
}
