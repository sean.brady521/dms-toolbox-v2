import { GeneratedNpc } from 'types/npcs'

export function combineDescription (npc: GeneratedNpc): string {
  if (npc.description && npc.ptraits) {
    if (npc.description.race === 'lizardman' || npc.description.race === 'lizardwoman') {
      npc.ptraits.traits1 = npc.ptraits.traitslizards
    }
    if (npc.description && npc.description.race === 'goliath') {
      npc.ptraits.traits1 = npc.ptraits.traitsgoliaths
    }
  }
  if (npc.description && npc.description.race === 'kenku') {
    npc.description.name = npc.description.kenkuname
  }

  const desc  = npc.description
  const physical = npc.physical
  const majP = desc.pronounCapit 
  let combined = ''
  combined += `${desc.name} is a ${desc.age} year old ${desc.gender} ${desc.race} ${desc.occupation}.\n`
  combined += `${majP}has ${physical.hair}${physical.eyes}.\n`
  combined += `${majP}has ${physical.skin}.\n`
  combined += `${majP}stands ${physical.height}cm tall and has a ${physical.build}.\n`
  combined += `${majP}has ${physical.face}\n`
  combined += `${physical.special1}\n`
  combined += `${physical.special2}\n`
  return combined
}

export function combinePersonalityTraits(npc: GeneratedNpc): string {
  const pQuirksDesc = npc.pquirks.description
  const quirksArray = pQuirksDesc.split('.')
  quirksArray.length--
  let combined = ''
  combined += `${npc.religion.description}\n`
  combined += `${npc.ptraits.traits1}\n`
  combined += `${npc.ptraits.traits2}\n`
  quirksArray.forEach(value => { combined += `${value}.` })
  return combined
}