import queryString from 'query-string'
import { Location } from '../types/common'



export default function parseLocation(location: Location): { route: string } {
  const parsedSearch = queryString.parse(location.search)
  return {
    route: location.pathname.replace('/', '') || 'home',
    ...parsedSearch
  }
}
