export function deepClone<T>(ob: T): T {
  return JSON.parse(JSON.stringify(ob))
}