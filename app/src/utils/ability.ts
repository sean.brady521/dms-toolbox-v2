export function getModifier (score: number): string {
  const value =  Math.floor((score - 10) / 2)
  return value >= 0 ? `+${value}` : `${value}`
}