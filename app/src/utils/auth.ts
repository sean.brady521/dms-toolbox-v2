import firebase from 'firebase/app'
import 'firebase/auth'
import { actionCodeSettings, firebaseConf } from 'config/firebase'
import { setLoading } from 'store/user/user-actions'

export function initialiseFirebase (): void {
  if(!isFirebaseInitialised()) firebase.initializeApp(firebaseConf)
}

export function isFirebaseInitialised (): boolean {
  return !!firebase.apps.length
}

export function getUserEmail (): string | undefined | null {
  initialiseFirebase()
  return firebase.auth().currentUser?.email
}

export async function sendSignOnEmail (email: string): Promise<void> {
  initialiseFirebase()
  await firebase.auth().sendSignInLinkToEmail(email, actionCodeSettings)
}

export async function getIdToken (): Promise<string | undefined> {
  initialiseFirebase()
  return await firebase.auth().currentUser?.getIdToken(true)
}

export function getUserId (): string | undefined {
  initialiseFirebase()
  return firebase.auth().currentUser?.uid
}

export async function logout (): Promise<void> {
  initialiseFirebase()
  await firebase.auth().signOut()
}

export async function checkEmailSignIn (onFinish: () => void): Promise<void> {
  initialiseFirebase()
  if (firebase.auth().isSignInWithEmailLink(window.location.href)) {
    let email = window.localStorage.getItem('emailForSignIn')

    if (!email) {
      email = window.prompt('Please provide your email for confirmation')
    }

    if (!email) return

    setLoading(true)
    
    const credential = await firebase.auth.EmailAuthProvider.credentialWithLink(email, window.location.href)
    await firebase.auth().currentUser?.linkWithCredential(credential)
      .then(() => {
        window.localStorage.removeItem('emailForSignIn')
        console.log('successfully joined anon data')
      })
      .catch((error) => {
        window.alert('This sign in link has already been used or is expired, please log in again.')
        console.error(error)
      })

    await firebase.auth().signInWithEmailLink(email, window.location.href)

    onFinish()
    setLoading(false)
  }
}