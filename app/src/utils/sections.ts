import { ISection, SectionScope } from 'types/common'
import { getUserId } from './auth'
import { v4 as uuidv4 } from 'uuid'
import { deepClone } from './clone'
import arrayMove from 'array-move'

export function getRootSection (scope: SectionScope, allSections?: ISection[]): ISection {
  if (allSections) {
    const rootSec = allSections.find(sec => sec.isRoot)
    if (rootSec) return rootSec
  }

  const userId = getUserId()
  if (!userId) throw new Error('No user id')

  return {
    id: `sections-root_${scope}_${userId}`,
    isRoot: true,
    createdBy: userId,
    collapsed: false,
    rank: 0,
    scope,
    itemIds: []
  }
}

export function getBlankSection (scope: SectionScope): ISection {
  const userId = getUserId()
  if (!userId) throw new Error('No user id')

  return {
    id: uuidv4(),
    isRoot: false,
    label: '',
    collapsed: false,
    createdBy: userId,
    rank: 1,
    scope,
    itemIds: []
  }
}

export function addBlankSection (scope: SectionScope, allSections: ISection[]): ISection[] {
  const modifiedSections = deepClone(allSections)

  const blankSection = getBlankSection(scope)

  modifiedSections.splice(1, 0, blankSection)

  modifiedSections.forEach((sec, idx) => sec.rank = idx)

  return modifiedSections
}

export function reorderSections (allSections: ISection[], oldIdx: number, newIdx: number): ISection[] {
  if (newIdx === 0 || oldIdx === 0) return [] // 0 is for root only
  const newSections = arrayMove(allSections, oldIdx, newIdx)
  newSections.forEach((sec, idx) => sec.rank = idx)
  return newSections
}