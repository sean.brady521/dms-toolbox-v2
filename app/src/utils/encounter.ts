import { Difficulty } from 'constants/enums'
import { ECreature } from 'types/encounters'
import { thresholds } from './encounter-generator/constants'

const crToXp: { [cr: number]: number } = {
  0: 10,
  0.125: 25,
  0.25: 50,
  0.5: 100,
  1: 200,
  2: 450,
  3: 700,
  4: 1100,
  5: 1800,
  6: 2300,
  7: 2900,
  8: 3900,
  9: 5000,
  10: 5900,
  11: 7200,
  12: 8400,
  13: 10000,
  14: 11500,
  15: 13000,
  16: 15000,
  17: 18000,
  18: 20000,
  19: 22000,
  20: 25000,
  21: 33000,
  22: 41000,
  23: 50000,
  24: 62000,
  25: 75000 
}

const monsterMultiplierTiers = [0.5, 1, 1.5, 2, 2.5, 3, 4, 5]

function getMonsterMultipler (numMonsters: number, numPcs: number): number {
  let resultTier = 0
  if (numMonsters === 1) {
    resultTier = 1 
  } else if (numMonsters === 2) {
    resultTier = 2
  } else if (numMonsters >= 3 && numMonsters <= 6) {
    resultTier = 3 
  } else if (numMonsters >= 7 && numMonsters <= 10) {
    resultTier = 4 
  } else if (numMonsters >= 11 && numMonsters <= 14) {
    resultTier = 5 
  } else if (numMonsters >= 15) {
    resultTier = 6 
  }

  if (numPcs < 3) resultTier++
  if (numPcs > 5) resultTier--

  return monsterMultiplierTiers[Math.max(resultTier, 0)]
}

export function getModifiedEncounterXp (monsterCrs: number[], numPcs: number): number {
  let encounterXp = 0
  for (const cr of monsterCrs) {
    encounterXp += crToXp[cr]
  }

  console.log({mult: getMonsterMultipler(monsterCrs.length, numPcs)})

  encounterXp *= getMonsterMultipler(monsterCrs.length, numPcs)
  return encounterXp
}

export function getEncounterDifficulty (monsterCrs: number[] = [], numPcs: number, avgLvl: number): Difficulty {
  const encounterXp = getModifiedEncounterXp(monsterCrs, numPcs)

  let difficulty = Difficulty.Easy
  const thresholdRow = getXpThresholds(avgLvl, numPcs)
  for (const xpThresh of thresholdRow) {
    if (encounterXp < xpThresh) {
      difficulty--
      break
    }
    difficulty++
  }
  return Math.max(Math.min(difficulty, Difficulty.Deadly), Difficulty.Easy)
}

export function getXpThresholds (avgLvl: number, numPcs: number): number[] {
  return thresholds[avgLvl - 1].map(xp => xp * numPcs)
}

export const difficultyToName = {
  [Difficulty.Easy]: 'Easy',
  [Difficulty.Medium]: 'Medium',
  [Difficulty.Difficult]: 'Difficult',
  [Difficulty.Deadly]: 'Deadly',
}

export function getXpBoundaries(avgLvl: number, numPcs: number): { label: string, value: number }[] {
  const boundaryRow = getXpThresholds(avgLvl, numPcs)
  return [
    { label: 'Easy', value: boundaryRow[0] },
    { label: 'Medium', value: boundaryRow[1] },
    { label: 'Difficult', value: boundaryRow[2] },
    { label: 'Deadly', value: boundaryRow[3] },
  ]
}

export function getEncounterCrs(creatures: ECreature[], creatureToCr: Record<string, { cr: number }>): number[] {
  const crs = []
  for (const crConf of creatures) {
    for (let i = 0; i < crConf.count; i++) {
      crs.push(creatureToCr[crConf.creature].cr)
    }
  }
  return crs
}