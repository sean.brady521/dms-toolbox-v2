let monsterTypes: string[]
let monsterEnvs: string[]

import { MonsterSize } from 'constants/enums'
import { GoodVsEvil, LawVsChaos, SearchQuery, SimpleMonster } from 'types/monster'
import monsters from 'utils/encounter-generator/monsters.json'

export function getMonsterTypes (): string[] {
  if (monsterTypes === undefined) {
    const types: Record<string, true> = {}
    for (const mon of monsters) {
      if (types[mon.type]) continue
      types[mon.type] = true
    }
    monsterTypes = Object.keys(types).sort()
  }
  return monsterTypes
}

export function getMonsterEnvs (): string[] {
  if (monsterEnvs === undefined) {
    const envs: Record<string, true> = {}
    for (const mon of monsters) {
      for (const env of mon.environments) {
        if (envs[env]) continue
        envs[env] = true
      }
    }
    monsterEnvs = Object.keys(envs).sort()
  }

  return monsterEnvs
}

export const monsterSizeToNum: Record<string, MonsterSize> = {
  Tiny: MonsterSize.Tiny,
  Small: MonsterSize.Small,
  Medium: MonsterSize.Medium,
  Large: MonsterSize.Large,
  Huge: MonsterSize.Huge,
  Gargantuan: MonsterSize.Gargantuan
}

function _monsterFitsQuery (monster: SimpleMonster, query: SearchQuery) {
  const { q, challenge, type, environments, alignment, size } = query

  if (q && q !== '' && !monster.name.toLowerCase().includes(q.toLowerCase())) {
    return false
  }

  if (challenge && (monster.cr < challenge.min || monster.cr > challenge.max)) {
    return false
  }
  
  if (type && !type.includes(monster.type)) {
    return false
  }

  if (environments && !monster.environments.some(env => environments.includes(env))) {
    return false
  }

  if (alignment && !(alignment.goodVsEvil.length === 0 && alignment.lawVsChaos.length === 0)) {
    const { goodVsEvil, lawVsChaos } = alignment
    if (monster.alignment === 'unaligned') {
      return false
    }

    const splitAlignment = monster.alignment.split(' ')
    const monLawVsChaos = splitAlignment[0] as LawVsChaos
    const monGoodVsEvil = splitAlignment.length === 1
      ? splitAlignment[0] as GoodVsEvil
      : splitAlignment[1] as GoodVsEvil

    if (goodVsEvil.length > 0 && lawVsChaos.length === 0 && !goodVsEvil.includes(monGoodVsEvil)) {
      return false
    } else if (goodVsEvil.length === 0 && lawVsChaos.length > 0 && !lawVsChaos.includes(monLawVsChaos)) {
      return false
    } else if ((goodVsEvil.length > 0 && lawVsChaos.length > 0) && (!goodVsEvil.includes(monGoodVsEvil) || !lawVsChaos.includes(monLawVsChaos))) {
      return false
    }
  }

  if (size && (monsterSizeToNum[monster.size] < size.min || monsterSizeToNum[monster.size] > size.max)) {
    return false
  }

  return true
}

export function selectFilteredMonsters (monsters: SimpleMonster[], query: SearchQuery): SimpleMonster[] {
  return monsters.filter(mon => _monsterFitsQuery(mon, query))
}
