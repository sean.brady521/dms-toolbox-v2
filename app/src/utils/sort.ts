/*
  creates a sort predicate using a function to map a value to a string
*/
export function alphaSortFn (func: <T>(val: T) => string, mult: number): <T>(a: T, b: T) => number {
  return <T>(a: T, b: T): number => {
    const aField = func(a).toLowerCase()
    const bField = func(b).toLowerCase()

    if (aField > bField) {
      return 1 * mult
    }

    if (aField < bField) {
      return -1 * mult
    }

    return 0
  }
}

/*
  creates a sort predicate using a key to map an object to a string
*/
export function alphaSort (key: string): <T>(a: T, b: T) => number {
  // eslint-disable-next-line
  // @ts-ignore
  return alphaSortFn(<T>(obj: T) => obj[key] || '')
}
