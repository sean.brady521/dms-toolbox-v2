export enum Difficulty {
  Easy = 1,
  Medium = 2,
  Difficult = 3,
  Deadly = 4
}

export enum Environment {
  Forest = 'forest',
  Desert = 'desert',
  Hill = 'hill',
  Grassland = 'grassland',
  Mountain = 'mountain',
  Arctic = 'arctic',
  Coastal = 'coastal',
  Swamp = 'swamp',
  Underdark = 'underdark',
  Underwater = 'underwater',
  Urban = 'urban',
  Planar = 'planar'
}

export enum MonsterSize {
  Tiny = 1,
  Small = 2,
  Medium = 3,
  Large = 4,
  Huge = 5,
  Gargantuan = 6
}