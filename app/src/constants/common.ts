export const abilityScores = [
  {
    label: 'STRENGTH',
    abbrev: 'str'
  },
  {
    label: 'DEXTERITY',
    abbrev: 'dex'
  },
  {
    label: 'CONSTITUTION',
    abbrev: 'con'
  },
  {
    label: 'INTELLIGENCE',
    abbrev: 'int'
  },
  {
    label: 'WISDOM',
    abbrev: 'wis'
  },
  {
    label: 'CHARISMA',
    abbrev: 'cha'
  },
] as const

export const rerollQuestions = [
  'Not a fan?',
  'Not feeling it?',
  'Bad one?',
  'Not looking good?',
  'Bad stuff?',
  'Weird one?',
  'Not loving it?'
]