import React from 'react'
import { Button } from 'semantic-ui-react'
import IconButton from 'components/icon-button'
import classNames from 'classnames'

import styles from './number-stepper.module.scss'

export interface NumberStepperProps {
  value: number
  className?: string
  min?: number
  max?: number
  vertical?: boolean
  onChange: (val: number) => void
}

const NumberStepper = (props: NumberStepperProps): JSX.Element => {
  const { value, min, max, vertical, className, onChange } = props

  const handleChange = (change: number): void => {
    const newVal = value + change

    if (min !== undefined && newVal < min) return
    if (max !== undefined && newVal > max) return

    onChange(newVal)
  }

  return (
    <div className={classNames(styles.root, vertical && styles.vertical, className)}>
      <IconButton icon='minus' className={styles.minus} onClick={() => handleChange(-1)} />
      <div className={classNames(styles.value, vertical && styles.valueVertical)}>
        {value}
      </div>
      <IconButton icon='plus' className={styles.plus} onClick={() => handleChange(1)} />
    </div>
  )
}

export default NumberStepper