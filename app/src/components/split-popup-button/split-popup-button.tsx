import React, { ReactNode } from 'react'
import { Button, Icon, Popup } from 'semantic-ui-react'
import StyledButton from 'components/button'
import styles from './split-popup-button.module.scss'

export interface PopupButtonProps {
  buttonText: string
  className?: string
  open?: boolean
  onClick?: () => void
  onOpen?: () => void
  onClose?: () => void
  children?: ReactNode
}

const SplitPopupButton = (props: PopupButtonProps): JSX.Element => {
  const { buttonText, children, className, open, onClick, onOpen, onClose } = props
  return (
    <Button.Group className={className}>
      <StyledButton className={styles.buttonLeft} onClick={onClick} >
        {buttonText}
      </StyledButton>
      <Popup 
        on='click'
        basic
        position='bottom right'
        className={styles.popup}
        onOpen={onOpen}
        onClose={onClose}
        open={open}
        trigger={(
          <StyledButton icon className={styles.buttonRight} >
            <Icon name='caret down' />
          </StyledButton>
        )} 
      >
        {children}
      </Popup>
    </Button.Group>
  )
}

export default SplitPopupButton