import React from 'react'
import classNames from 'classnames'

import styles from './checkbox.module.scss'
import { Icon } from 'semantic-ui-react'

export interface CheckboxProps {
  label: string
  checked: boolean
  className?: string
  onToggle: () => void
}

const Checkbox = (props: CheckboxProps): JSX.Element => {
  const { label, checked, className, onToggle } = props

  return (
    <div className={classNames(styles.root, className)} onClick={onToggle}>
      <div className={styles.boxWrap}>
        <span className={styles.coupling}>
          <span className={classNames(styles.box, checked && styles.checked)}>
            {checked && <Icon name='check' className={styles.check} />}
          </span>
          <input type='checkbox' className={styles.input} />
        </span>
      </div>
      <div className={styles.label}>
        {label}
      </div>
    </div>
  )
}

export default Checkbox