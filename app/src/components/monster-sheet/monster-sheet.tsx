import React from 'react'
import { Dimmer, Icon, Loader, Modal } from 'semantic-ui-react'
import { MonsterData } from 'types/monster'
import styles from './monster-sheet.module.scss'
import { getModifier } from 'utils/ability'
import { abilityScores } from 'constants/common'

export interface ConfigModalProps {
  open: boolean
  onClose: () => void
  loading?: boolean
  monster: MonsterData | null
}

const Divider = (): JSX.Element => (
  <div className={styles.divider} />
)

const SectionHeader = (props: { name: string }): JSX.Element => (
  <div className={styles.sectionHeader}>
    {props.name}
  </div>
)

const KeyVals = (props: { pairs: { field: string, val: string }[] }): JSX.Element => (
  <div className={styles.keyVal}>
    {props.pairs.map((pair, idx) => (
      <>
        <div className={styles.field} key={`${idx}-${pair.field}`}>
          {pair.field}
        </div>
        <div className={styles.val} key={`${idx}-${pair.val}`}>
          {pair.val}
        </div>
      </>
    ))}
  </div>
)

const AbilityScore = (props: { name: string, score: number }): JSX.Element => {
  return (
    <div className={styles.abilityScore}>
      <div className={styles.abbrev}>
        {props.name}
      </div>
      <div className={styles.score}>
        {`${props.score} (${getModifier(props.score)})`}
      </div>
    </div>
  )
}

function speedToString (speed: Record<string, string>) {
  const speeds: string[] = []
  for (const [type, dist] of Object.entries(speed)) {
    speeds.push(`${type} ${dist.replace('ft.', 'ft')}`)
  }
  return speeds.join(', ')
}

function getPeripheralInfo(monster: MonsterData): { field: string, val: string }[] {
  const saving = []
  const skills = []
  const senses = []

  const saveRgx = /^saving throw:\s*/i
  const skillRgx = /^skill:\s*/i
  for (const prof of monster.proficiencies) {
    const { proficiency, value } = prof
    if (saveRgx.test(proficiency.name)) {
      saving.push(`${proficiency.name.replace(saveRgx, '')} + ${value}`)
    }
    if (skillRgx.test(proficiency.name)) {
      skills.push(`${proficiency.name.replace(skillRgx, '')} + ${value}`)
    }
  }

  for (const [sense, score] of Object.entries(monster.senses)) {
    senses.push(`${sense.replace('_', ' ')} ${`${score}`.replace('ft.', 'ft')}`)
  }

  const info = []
  if (saving.length) {
    info.push({
      field: 'Saving Throws',
      val: saving.join(', ')
    })
  }

  if (skills.length) {
    info.push({
      field: 'Skills',
      val: skills.join(', ')
    })
  }

  if (monster.damage_resistances?.length) {
    info.push({
      field: 'Damage Resistances',
      val: monster.damage_resistances.join(', ')
    })
  }

  if (monster.damage_immunities?.length) {
    info.push({
      field: 'Damage Immunities',
      val: monster.damage_immunities.join(', ')
    })
  }

  if (monster.condition_immunities?.length) {
    info.push({
      field: 'Condition Immunities',
      val: monster.condition_immunities.map(({ name }) => name).join(', ')
    })
  }

  if (monster.damage_vulnerabilities?.length) {
    info.push({
      field: 'Damage Vulnerabilities',
      val: monster.damage_vulnerabilities.join(', ')
    })
  }

  if (senses.length) {
    info.push({
      field: 'Senses',
      val: senses.join(', ')
    })
  }

  if (monster.languages?.length) {
    info.push({
      field: 'Languages',
      val: monster.languages
    })
  }

  info.push({
    field: 'Challenge',
    val: `${monster.challenge_rating} (${monster.xp} XP)`
  })

  return info
}

function getHitPointStr (monster: MonsterData) {
  const numHitDie = parseInt(monster.hit_dice.split('d')[0])
  const modifier = numHitDie * parseInt(getModifier(monster.constitution))
  return `${monster.hit_points} (${monster.hit_dice} + ${modifier})`
}

function getLegendayText (type: string, numActions: number) {
  return `The ${type} can take ${numActions} legendary actions, choosing from the options below. Only one legendary action option can be used at a time and only at the end of another creature's turn. The ${type} regains spent legendary actions at the start of its turn.`
}

const MonsterSheet = (props: ConfigModalProps): JSX.Element => {
  const { open, monster, loading, onClose } = props

  return (
    <Modal 
      open={open} 
      closeIcon={<Icon name='close' />}
      className={styles.modal} 
      onClose={onClose} 
    >
      {(monster === null && !loading) && (
        <div>
          {'Couldn\'t find this one :('}
        </div>
      )}
      {(monster === null && loading) && (
        <div className={styles.loaderWrap}>
          <Dimmer active inverted>
            <Loader active inline='centered' />
          </Dimmer>
        </div>
      )}
      {monster !== null &&
        <div className={styles.inner}>
          <div className={styles.section}>
            <h1 className={styles.name}>{monster.name}</h1>
            <div className={styles.subHeader}>
              {`${monster.size} ${monster.type}${monster.subtype ? ` (${monster.subtype})` : ''}, ${monster.alignment}`}
            </div>
            <Divider />
          </div>
          <div className={styles.section}>
            <KeyVals 
              pairs={[
                { field: 'Armor Class', val: `${monster.armor_class}`},
                { field: 'Hit Points', val: getHitPointStr(monster)},
                { field: 'Speed', val: speedToString(monster.speed) }
              ]} 
            />
            <Divider />
          </div>
          <div className={styles.section}>
            <div className={styles.abilityScores}>
              {abilityScores.map(abScore => (
                <AbilityScore 
                  name={abScore.abbrev} 
                  score={monster[abScore.label.toLowerCase() as keyof MonsterData] as number} 
                  key={abScore.abbrev} 
                />
              ))}
            </div>
            <Divider />
          </div>
          <div className={styles.section}>
            <KeyVals 
              pairs={getPeripheralInfo(monster)} 
            />
            <Divider />
          </div>
          <div className={styles.section}>
            <div className={styles.specialAbilities}>
              {monster.special_abilities?.map(ability => (
                <div key={ability.name} className={styles.ability}>
                  <span className={styles.abilityName}>{ability.name}. </span>
                  {ability.desc}
                </div>
              ))}
            </div>
          </div>
          <div className={styles.section}>
            <SectionHeader name='Actions' />
            <div className={styles.specialAbilities}>
              {monster.actions?.map(action => (
                <div key={action.name} className={styles.ability}>
                  <span className={styles.abilityName}>{action.name}. </span>
                  {action.desc}
                </div>
              ))}
            </div>
          </div>
          {monster.reactions?.length && 
            <div className={styles.section}>
              <SectionHeader name='Reactions' />
              <div className={styles.specialAbilities}>
                {monster.reactions?.map(reaction => (
                  <div key={reaction.name} className={styles.ability}>
                    <span className={styles.abilityName}>{reaction.name}. </span>
                    {reaction.desc}
                  </div>
                ))}
              </div>
            </div>
          }
          {monster.legendary_actions?.length && 
            <div className={styles.section}>
              <SectionHeader name='Legendary Actions' />
              <div className={styles.legendaryText}>
                {getLegendayText(monster.type, monster.legendary_actions.length)}
              </div>
              <div className={styles.specialAbilities}>
                {monster.legendary_actions?.map(action => (
                  <div key={action.name} className={styles.ability}>
                    <span className={styles.abilityName}>{action.name}. </span>
                    {action.desc}
                  </div>
                ))}
              </div>
            </div>
          }
        </div>
      }
    </Modal>
  )
}

export default MonsterSheet