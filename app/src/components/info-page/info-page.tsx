import React from 'react'
import { Icon, SemanticICONS } from 'semantic-ui-react'
import classNames from 'classnames'
import styles from './info-page.module.scss'

interface MenuItem {
  icon: SemanticICONS
  label: string
  onClick: () => void
}

export interface InfoPageProps {
  onBack: () => void
  title: string
  menuItems?: MenuItem[]
  className?: string
  children: React.ReactNode
}

const InfoPage = (props: InfoPageProps): JSX.Element => {
  const { onBack, className, title, menuItems, children } = props
  return (
    <div className={classNames(styles.root, className)}>
      <div className={styles.topSec}>
        <div className={styles.backAndTitle} onClick={onBack}>
          <div className={styles.backWrap}>
            <Icon name='chevron left' className={styles.backIcon} />
          </div>
          <h2 className={styles.title}>
            {title}
          </h2>
        </div>
        <div className={styles.menuWrap}>
          {menuItems?.map((item, idx) => (
            <div key={idx} className={styles.menuItem} onClick={item.onClick}>
              <Icon name={item.icon} className={styles.menuItemIcon} />
              {item.label}
            </div>
          ))}
        </div>
      </div>
      {children}
    </div>
  )
}

export default InfoPage