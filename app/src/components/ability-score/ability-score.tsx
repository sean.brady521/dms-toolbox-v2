import React from 'react'
import classNames from 'classnames'
import styles from './ability-score.module.scss'
import { Input } from 'semantic-ui-react'
import { getModifier } from 'utils/ability'

interface AbilityScoreProps {
  name: string
  value?: number
  edittable?: boolean
  className?: string
  onChange?: (newVal: string) => void
}

const AbilityScore = (props: AbilityScoreProps): JSX.Element => {
  const { name, value, edittable, className, onChange } = props

  const handleChange = (value: string) => {
    if (!onChange) return
    const parsedVal = parseInt(value)
    const result = isNaN(parsedVal)
      ? '' 
      : `${Math.min(Math.max(parsedVal, 0), 30)}`
    onChange(result)
  }

  return (
    <div className={classNames(styles.root, className)}>
      <img alt='ability-frame' src='https://i.ibb.co/5RYx21j/ability-Frame.png'/> 
      <div className={styles.name}>
        {name}
      </div>
      <div className={styles.value}>
        {edittable && (
          <Input 
            value={value} 
            className={styles.input}
            onChange={(_, { value }) => handleChange(value)} 
            type='number'
            transparent
          />
        )}
        <div className={styles.noInput}>
          {!edittable && value}
        </div>
      </div>
      <div className={styles.modifier}>
        {getModifier(value || 0)}
      </div>
    </div>
  )
}

export default AbilityScore