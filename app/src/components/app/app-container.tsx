import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { ISection, StoreState, } from 'types/common'
import App, { AppProps } from './app'
import { initAuthManager } from 'store/user/user-actions'
import { initialiseFirebase } from 'utils/auth'
import { Dispatch } from 'redux'
import { fetchNpcs, postNpc } from 'store/npcs/npcs-actions'
import { fetchEncounters, postEncounter } from 'store/encounters/encounters-actions'
import { fetchSections, postSections } from 'store/sections/sections-actions'
import { getSampleNpc } from 'utils/npc-generator/npc-generator'
import { getSampleEncounter } from 'utils/encounter-generator/generator'
import { getBlankSection, getRootSection } from 'utils/sections'

interface AppContainerProps extends AppProps {
  dataExists: boolean
  hasSeenSamples: boolean
  startAuthManager: () => void
  addSamples: () => void
}

export const AppContainer = (props: AppContainerProps): JSX.Element => {
  const { startAuthManager, addSamples, dataExists, hasSeenSamples, loading, fetching } = props

  useEffect(() => {
    initialiseFirebase()
    startAuthManager()
  }, [])

  useEffect(() => {
    if (!loading && !fetching && !dataExists && !hasSeenSamples) {
      addSamples()
    }
  }, [loading, fetching, dataExists, hasSeenSamples])

  return <App loading={loading} fetching={fetching} />
}

export function mapStateToProps(state: StoreState): Pick<AppContainerProps, 'loading' | 'fetching' | 'dataExists' | 'hasSeenSamples'> {
  const { user, npcs, encounters, sections } = state

  const hasSeenSamples = localStorage.getItem('seen_samples') === '1'

  return { 
    loading: user.loading,
    fetching: !npcs.hasFetched || !encounters.hasFetched,
    dataExists: Object.keys(npcs.npcs).length > 0 || Object.keys(encounters.encounters).length > 0 || sections.sections.length > 0,
    hasSeenSamples
  }
}

export function mapDispatchToProps(dispatch: Dispatch): Pick<AppContainerProps, 'startAuthManager' | 'addSamples'> {
  const prefetchData = () => { 
    fetchNpcs()(dispatch)
    fetchEncounters()(dispatch)
    fetchSections()(dispatch)
  }

  const startAuthManager = async () => {
    await initAuthManager(prefetchData)(dispatch)
  }

  const addSamples = () => {
    const npcSample = getSampleNpc()
    const encounterSample = getSampleEncounter()

    const npcRootSection = getRootSection('npcs')
    const encountersRootSection = getRootSection('encounters')

    const sectionNpc: ISection = { 
      ...getBlankSection('npcs'),
      label: 'First NPC collection',
      itemIds: [npcSample.id]
    }

    const sectionEncounter: ISection = { 
      ...getBlankSection('encounters'),
      label: 'First encounter collection',
      itemIds: [encounterSample.id]
    }

    postNpc(npcSample)(dispatch)
    postEncounter(encounterSample)(dispatch)
    postSections([npcRootSection, encountersRootSection, sectionNpc, sectionEncounter])(dispatch)

    localStorage.setItem('seen_samples', '1')
  }

  return {
    startAuthManager,
    addSamples 
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AppContainer)