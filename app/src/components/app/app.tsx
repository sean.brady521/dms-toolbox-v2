import React, { useEffect } from 'react'
import { Route, Switch } from 'react-router-dom'
import styles from './app.module.scss'

import Sidebar from 'components/sidebar'
import HomePage from 'routes/home'
import NpcsPage from 'routes/npcs'
import EncountersPage from 'routes/encounters'
import NpcPage from 'routes/npc'
import EncounterPage from 'routes/encounter'
import CreateEncounterPage from 'routes/create-encounter'

import FullPageLoad from 'components/full-page-load'

export interface AppProps {
  loading: boolean
  fetching: boolean
}

const App = (props: AppProps): JSX.Element => {
  const { loading, fetching } = props

  const appLoading = loading || fetching

  return (
    <div className={styles.root}>
      <Sidebar />
      <Switch>
        {appLoading && <Route path='*' component={FullPageLoad} />}
        <Route exact path='/' component={HomePage} />
        <Route exact path='/home' component={HomePage} />
        <Route exact path='/npc/:id' component={NpcPage} />
        <Route exact path='/npcs' component={NpcsPage} />
        <Route exact path='/encounters' component={EncountersPage} />
        <Route exact path='/create-encounter' component={CreateEncounterPage} />
        <Route exact path='/encounter/:id' component={EncounterPage} />
      </Switch>
    </div>
  )
}
export default App
