import React, { ReactNode } from 'react'
import { Icon, Placeholder, Popup, SemanticICONS } from 'semantic-ui-react'
import classNames from 'classnames'
import styles from './info-block.module.scss'
import { ReactComponent as DiceIcon } from 'assets/icons/dice.svg'

export interface InfoBlockItem {
  icon?: SemanticICONS
  tooltip?: string
  content: string
}

export interface InfoBlockProps {
  title: string
  items?: InfoBlockItem[]
  badge?: string
  children?: ReactNode
  className?: string
  contentClassName?: string
  showPlaceholder?: boolean
  onEdit?: (newItems: string[]) => void
  onRandomise?: () => void
  onClick?: () => void
}

const InfoBlock = (props: InfoBlockProps): JSX.Element => {
  const { title, items, badge, children, className, showPlaceholder, contentClassName, onEdit, onRandomise, onClick } = props

  const handleEdit = () => {
    console.log('edit')
  }

  const menuItems = []
  if (onRandomise) {
    menuItems.push(
      <Popup 
        content='Randomise collection' 
        trigger={ <Icon circular name='random' className={styles.editIcon} onClick={onRandomise} /> }
      />
    )
  }
  
  if (onEdit) {
    menuItems.push(
      <Popup 
        content='Manually edit collection' 
        trigger={ <Icon circular name='edit' className={styles.editIcon} onClick={handleEdit} /> }
      />
    )
  }

  return (
    <div className={classNames(styles.root, className)} onClick={onClick}>
      {badge && <div className={styles.badge}>{badge}</div>}
      <div className={styles.cardTop}>
        <h2 className={styles.title}>{title}</h2>
        {menuItems.length > 0 && 
          <div className={styles.cardMenu}>
            {menuItems}
          </div>
        }
      </div>
      {(!children && !!items) &&
        <div className={classNames(styles.items, contentClassName)}>
          {items?.map((item, idx) => (
            <div key={idx} className={styles.itemWrap}>
              {item.icon && <Icon name={item.icon} className={styles.itemIcon} />}
              {!showPlaceholder && 
                <div className={styles.itemContent}>
                  {item.content}
                </div>
              }
              {showPlaceholder && 
                <Placeholder className={styles.itemContentPlaceholder}>
                  <Placeholder.Paragraph />
                </Placeholder>
              }
            </div>
          ))}
        </div>
      }
      {!!children && children}
    </div>
  )
}

export default InfoBlock