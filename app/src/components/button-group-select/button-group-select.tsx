import React from 'react'
import { SemanticICONS } from 'semantic-ui-react'
import styles from './button-group-select.module.scss'
import classNames from 'classnames'

export interface ButtonConfig {
  value: string, 
  label: string, 
  icon?: SemanticICONS
}

export interface ButtonGroupSelectProps {
  selected: string
  buttons: ButtonConfig[]
  className?: string
  onClick: (val: string) => void
}

const ButtonGroupSelect = (props: ButtonGroupSelectProps): JSX.Element => {
  const { selected, buttons, onClick, className } = props
  return (
    <div className={classNames(styles.root, className)}>
      {buttons.map(but => (
        <div 
          key={but.value} 
          onClick={() => onClick(but.value)} 
          className={classNames(styles.button, selected === but.value && styles.selected)}
        >
          {but.label} 
        </div>
      ))}
    </div>
  )
}

export default ButtonGroupSelect