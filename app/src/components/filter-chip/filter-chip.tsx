import React, { useState } from 'react'
import { Divider, Popup } from 'semantic-ui-react'
import styles from './filter-chip.module.scss'
import Button from 'components/button'
import classNames from 'classnames'

interface ChipProps {
  text: string
  active?: boolean
  onClick: () => void
}
const Chip = (props: ChipProps) => { 
  const { onClick, text, active } = props
  return (
    <div className={classNames(styles.chip, active && styles.chipActive)} onClick={onClick}>
      {text}
    </div>
  )
}

export interface FilterChipProps {
  label: string
  displayValue?: string
  contentClassName?: string
  active?: boolean
  children: React.ReactNode
  onOpen?: () => void
  onClose?: () => void
  onSave: () => void
  onClear: () => void
}

const FilterChip = (props: FilterChipProps): JSX.Element => {
  const { 
    label,
    displayValue,
    active,
    children,
    contentClassName,
    onOpen,
    onClose,
    onSave,
    onClear 
  } = props

  const [open, setOpen] = useState(false)

  return (
    <Popup 
      className={styles.popup} 
      trigger={
        <Chip 
          active={active || open}
          text={(displayValue && displayValue.length < 20) ? displayValue : label} 
          onClick={() => setOpen(true)} 
        />
      }
      position='bottom left'
      on='click'
      basic
      open={open}
      onClose={() => { 
        setOpen(false)
        onClose && onClose()
      }}
      onOpen={() => { 
        setOpen(true)
        onOpen && onOpen()
      }}
    >
      <div className={styles.inner}>
        <div className={classNames(styles.childWrap, contentClassName)}>
          {children}
        </div>
        <div className={styles.bottomWrap}>
          <Divider className={styles.divider} />
          <div className={styles.saveAndClear}>
            <div className={classNames(styles.clear, !active && styles.disabled)} onClick={() => active && onClear()}>
              Clear 
            </div>
            <Button 
              content='Save' 
              size='mini'
              className={styles.save} 
              onClick={() => {
                onSave()
                setOpen(false)
              }}
            />
          </div>
        </div>
      </div>
    </Popup>
  )
}

export default FilterChip