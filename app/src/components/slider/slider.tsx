import React from 'react'
import classNames from 'classnames'
import styles from './slider.module.scss'
import MuiSlider, { SliderProps as MuiSliderProps } from '@material-ui/core/Slider'

interface SliderProps extends Omit<MuiSliderProps, 'onChange'> {
  onChange: (newVal: number | number[]) => void
}

const Slider = (props: SliderProps): JSX.Element => {
  const { className, onChange, ...otherProps } = props

  return (
    <MuiSlider 
      className={classNames(styles.slider, className)}
      onChange={(_: any, newVal: number | number[]) => {
        onChange(newVal)
      }}
      {...otherProps}
    />
  )
}

export default Slider