import React, { useState } from 'react'
import classNames from 'classnames'

import PopupButton from 'components/popup-button'
import { Option } from 'types/common'

import styles from './menu-button.module.scss'
import { Icon, PopupProps, SemanticICONS } from 'semantic-ui-react'

interface MenuOption extends Option {
  group?: string
  subLabel?: string
}

export interface MenuButtonProps {
  options: MenuOption[]
  buttonText: string
  buttonIcon?: SemanticICONS
  className?: string
  position?: PopupProps['position']
  onSelect: (val: string) => void
}

function getOptionsByGroup (options: MenuOption[]) {
  const optionsByGroup: Record<string, MenuOption[]> = {}
  for (const op of options) {
    const group = op.group || 'no group'
    if (!optionsByGroup[group]) optionsByGroup[group] = []
    optionsByGroup[group].push(op)
  }
  return optionsByGroup
}

const MenuButton = (props: MenuButtonProps): JSX.Element => {
  const { options, position, buttonText, className, buttonIcon, onSelect } = props

  const [open, setOpen] = useState<boolean>(false)

  const byGroup = getOptionsByGroup(options)

  return (
    <PopupButton 
      open={open}
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      buttonText={buttonText} 
      className={className} 
      position={position} 
      buttonIcon={buttonIcon}
    >
      <div className={styles.optionsWrap}>
        {Object.keys(byGroup).map(opGroup => (
          <div key={opGroup} className={styles.opGroup}>
            {opGroup !== 'no group' && (
              <div className={styles.groupHeading}>
                {opGroup}
              </div>
            )}
            {byGroup[opGroup].map(op => (
              <div 
                key={op.value} 
                className={styles.option} 
                onClick={() => { 
                  setOpen(false)
                  onSelect(op.value)
                }}
              >
                {op.icon && <Icon name={op.icon} className={styles.opIcon} size='large' />}
                <div className={styles.opTextWrap}>
                  <div className={classNames(styles.opLabel, op.subLabel && styles.opPrimaryLabel)}>
                    {op.label}
                  </div>
                  {op.subLabel && (
                    <div className={styles.opSubLabel}>
                      {op.subLabel}
                    </div> 
                  )}
                </div>
              </div>
            ))}
          </div>
        ))}
      </div>
    </PopupButton>
  )
}

export default MenuButton
