import React from 'react'
import { Button, ButtonProps } from 'semantic-ui-react'
import classNames from 'classnames'
import styles from './button.module.scss'

// export interface CustomButtonProps extends ButtonProps {

// }

const CustomButton = (props: ButtonProps): JSX.Element => {
  const { secondary, className, ...buttonProps} = props
  return (
    <Button 
      className={classNames(styles.button, !secondary && styles.primary, secondary && styles.secondary, className)} 
      {...buttonProps} 
    />
  )
}

export default CustomButton