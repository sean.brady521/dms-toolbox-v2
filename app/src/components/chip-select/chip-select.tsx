import React from 'react'
import classNames from 'classnames'

import { Option } from 'types/common'

import styles from './chip-select.module.scss'

type ChipOption = Pick<Option, 'value' | 'label'>

export interface ChipSelectProps {
  className?: string
  chipClassName?: string
  selected: string[]
  options: ChipOption[]
  onChange: (selected: string[]) => void
}

const ChipSelect = (props: ChipSelectProps): JSX.Element => {
  const { selected, options, className, chipClassName, onChange } = props

  const handleSelect = (newVal: string) => {
    let newSelected
    if (selected.includes(newVal)) {
      newSelected = selected.filter(v => v !== newVal)
    } else {
      newSelected = [...selected, newVal]
    }
    onChange(newSelected)
  }


  return (
    <div className={classNames(styles.root, className)}>
      {options.map(op => (
        <div 
          className={classNames(styles.chip, selected.includes(op.value) && styles.selected, chipClassName)} 
          onClick={() => handleSelect(op.value)}
          key={op.value}
        >
          {op.label}
        </div>
      ))}
    </div>
  )
}

export default ChipSelect