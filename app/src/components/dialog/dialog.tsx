import React from 'react'
import styles from './dialog.module.scss'
import { Modal, Button, Icon } from 'semantic-ui-react'

interface DialogProps {
  open: boolean
  message: string
  onClose: () => void

  primaryText: string
  onPrimary: () => void

  secondaryText?: string
  secondaryIsPrimary?: boolean
  onSecondary?: () => void
}

const Dialog = (props: DialogProps): JSX.Element => {
  const {
    open, 
    message, 
    onClose, 
    primaryText,
    onPrimary, 
    secondaryText,
    secondaryIsPrimary,
    onSecondary
  } = props

  return (
    <Modal
      size='tiny'
      open={open}
      closeIcon={<Icon name='close' />}
      className={styles.root} 
      onClose={onClose}
    >
      <div className={styles.message}>
        {message}
      </div>
      <div className={styles.buttons}>
        <Button 
          className={styles.button}
          onClick={onPrimary}
        >
          {primaryText}
        </Button>
        {(!!onSecondary && !!secondaryText) &&
          <Button 
            className={styles.button}
            basic={!secondaryIsPrimary}
            onClick={onSecondary}
          >
            {secondaryText}
          </Button>}
      </div>
    </Modal>
  ) 
}

export default Dialog