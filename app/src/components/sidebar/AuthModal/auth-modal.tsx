import React, { useState } from 'react'
import { Divider, Icon, Modal } from 'semantic-ui-react'

import Button from 'components/button'
import Input from 'components/input'

import styles from './auth-modal.module.scss'

export interface AuthModalProps {
  open: boolean
  onClose: () => void
  checkAccountExists: (email: string) => Promise<boolean>
  onSignUp: (email: string) => void
}

enum Stage {
  Init = 'init',
  UserInfo = 'user-info',
  EmailSent = 'email-sent'
}

const AuthModal = (props: AuthModalProps): JSX.Element => {
  const {
    open,
    onClose,
    onSignUp,
    checkAccountExists
  } = props

  const [stage, setStage] = useState<Stage>(Stage.Init)
  const [email, setEmail] = useState<string>('')
  // const [name, setName] = useState<string>('')
  const [submitDidFail, setSubmitDidFail] = useState(false)

  const handleClose = () => {
    setEmail('')
    // setName('')
    setStage(Stage.Init)
    setSubmitDidFail(false)
    onClose()
  }

  return (
    <Modal 
      open={open} 
      onClose={handleClose}
      size='tiny'
      closeIcon={<Icon name='close' />}
    >
      {stage === 'init' && (
        <>
          <h2 className={styles.header}>
          Log in or sign up
          </h2>
          <Divider className={styles.divider} />
          <div className={styles.formWrap}>
            <h3 className={styles.welcome}>
              {'Welcome to DM\'s Toolbox'}
            </h3>
            <Input 
              value={email}
              onChange={(e, { value }) => setEmail(value)}
              autoFocus
              placeholder='email'
              error={submitDidFail && !email}
              className={styles.input}
            />
            <Button 
              fluid 
              className={styles.continue}
              onClick={async () => {
                if (!email) {
                  setSubmitDidFail(true)
                  return
                }
                else setSubmitDidFail(false)

                // const exists = await checkAccountExists(email)
                // setStage(exists ? Stage.EmailSent : Stage.UserInfo)
                onSignUp(email)
                setStage(Stage.EmailSent)
              }}
            >
              Continue
            </Button>
          </div>
        </>
      )}
      {/* {stage === Stage.UserInfo && (
        <>
          <h2 className={styles.header}>
            Finish signing up
          </h2>
          <Divider className={styles.divider} />
          <div className={styles.formWrap}>
            <div className={styles.inputWrap}>
              <Input 
                value={name}
                onChange={(e, { value }) => setName(value)}
                autoFocus
                error={submitDidFail && !name}
                placeholder='Name/Alias'
                className={styles.inputWsubtext}
              />
              <div className={styles.inputSubtext}>
                This is just so we know what to call you in the app!
              </div>
            </div>
            <div className={styles.inputWrap}>
              <Input
                value={email}
                onChange={(e, { value }) => setEmail(value)}
                placeholder='Email'
                error={submitDidFail && !email}
                type='email'
                className={styles.inputWsubtext}
              />
              <div className={styles.inputSubtext}>
                We won’t send you any emails besides your initial confirmation.
              </div>
            </div>
            <Button 
              fluid 
              className={styles.continue}
              onClick={async () => {
                if (!email || !name) { 
                  setSubmitDidFail(true)
                  return
                }
                onSignUp(name, email)
                setStage(Stage.EmailSent)
              }}
            >
              Sign up
            </Button>
          </div>
        </>
      )} */}
      {stage === Stage.EmailSent && (
        <>
          <h2 className={styles.header}>
            No passwords required!
          </h2>
          <Divider className={styles.divider} />
          <div className={styles.formWrap}>
            <div className={styles.codeInfo}>
              {`We’ve sent an email with a login link to ${email}.`}
            </div>
            <Button 
              fluid 
              className={styles.continue}
              onClick={async () => {
                onClose()
              }}
            >
              {'I\'ll check my email'}
            </Button>
          </div>
        </>
      )}

    </Modal>
  )
}

export default AuthModal