import React from 'react'
import { Dispatch } from 'redux'
import { connect } from 'react-redux'
import { replace } from 'connected-react-router'

import { StoreState, } from 'types/common'

import Sidebar, { SidebarProps } from './sidebar'
import pages from 'config/pages'
import parseLocation from 'utils/parse-location'

const noSidebarRoutes = ['create-encounter']

export const SidebarContainer = (props: SidebarProps): JSX.Element => {
  return <Sidebar {...props} />
}

export function mapStateToProps(state: StoreState): Pick<SidebarProps, 'pages' | 'currentPage' | 'hide'> {
  const { route } = parseLocation(state.router.location)
  return { 
    hide: noSidebarRoutes.includes(route),
    pages,
    currentPage: route
  }
}

export function mapDispatchToProps(dispatch: Dispatch): Pick<SidebarProps, 'changePage'> {
  return {
    changePage: (page: string): void => {
      dispatch(replace(`/${page}`))
    },
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SidebarContainer)