import React from 'react'
import classNames from 'classnames'

import styles from './sidebar.module.scss'
import toolsImage from 'assets/images/tools.png'

import { ReactComponent as HomeIcon } from 'assets/icons/cottage.svg'
import { ReactComponent as FireIcon } from 'assets/icons/fire.svg'
import { ReactComponent as GroupIcon } from 'assets/icons/groups.svg'

import Account from './components/account'


const PageIcon = (icon: string): JSX.Element => {
  switch (icon) {
  case 'home':
    return <HomeIcon className={styles.icon} />
  case 'fire':
    return <FireIcon className={styles.icon} />
  case 'group':
    return <GroupIcon className={styles.icon} />
  default:
    return <div />
  }
}

export interface SidebarProps {
  currentPage: string
  hide: boolean
  pages: { label: string, value: string, icon: string }[]
  changePage: (page: string) => void
}

const Sidebar = (props: SidebarProps): JSX.Element => {
  const { currentPage, pages, hide, changePage } = props

  if (hide) return <div />

  return (
    <div className={styles.root}>
      <div className={styles.brand}>
        <img src={toolsImage} className={styles.toolsImage} />
        {'DM\'s Toolbox'}
      </div>
      <div className={styles.pages}>
        {pages.map(p => { 
          return (
            <div 
              key={p.value} 
              className={classNames(styles.pageItem, p.value === currentPage && styles.selected)} 
              onClick={() => changePage(p.value)}
            >
              {PageIcon(p.icon)}
              {p.label}
            </div>
          )})}
      </div>
      <div className={styles.login}>
        <Account />
      </div>
    </div>
  )
}

export default Sidebar