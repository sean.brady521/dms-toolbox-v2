import React from 'react'
import { connect } from 'react-redux'

import { StoreState, } from 'types/common'

import Account, { AccountProps } from './account'
import { getUserEmail, logout } from 'utils/auth'
import { Dispatch } from 'redux'
import { setLoggedIn } from 'store/user/user-actions'

export const AccountContainer = (props: AccountProps): JSX.Element => {
  return <Account {...props} />
}

export function mapStateToProps(state: StoreState): Pick<AccountProps, 'email' | 'loggedIn' | 'checkAccountExists'> {
  const { loggedIn, anonymous } = state.user

  return { 
    email: getUserEmail() || undefined,
    loggedIn: loggedIn && !anonymous,
    checkAccountExists: (email: string) => {
      return Promise.resolve(false)
    }
  }
}

export function mapDispatchToProps(dispatch: Dispatch): Pick<AccountProps, 'onLogout'> {
  return {
    onLogout: () => { 
      dispatch(setLoggedIn(false))
      logout()
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AccountContainer)