import React, { useState } from 'react'
import styles from './account.module.scss'
import Button from 'components/button'
import AuthModal from 'components/sidebar/AuthModal'
import { sendSignOnEmail } from 'utils/auth'

export interface AccountProps {
  loggedIn: boolean
  email?: string
  onLogout: () => void
  checkAccountExists: (email: string) => Promise<boolean>
}

const Account = (props: AccountProps): JSX.Element => {
  const { loggedIn, email, onLogout, checkAccountExists } = props

  const [showAuthModal, setShowAuthModal] = useState(false)

  return (
    <div className={styles.root}>
      <AuthModal 
        open={showAuthModal} 
        onClose={() => setShowAuthModal(false)}
        onSignUp={(email) => {
          window.localStorage.setItem('emailForSignIn', email)
          sendSignOnEmail(email)
        }}
        checkAccountExists={() => Promise.resolve(false)}
      />
      {loggedIn && (
        <div className={styles.loggedInMenu}>
          <div className={styles.avatarLetter}>
            {email?.[0] || '?'}
          </div>
          <div className={styles.emailAndLogout}>
            <div className={styles.email}>
              {email}
            </div>
            <div className={styles.logout} onClick={onLogout}>
              logout
            </div>
          </div>
        </div>
      )}
      {!loggedIn && (
        <Button 
          basic 
          onClick={(): void => setShowAuthModal(true)} 
          fluid 
          className={styles.signUp}
        >
            Log in / Sign up
        </Button>
      )}
    </div>
  )
}

export default Account