import React from 'react'
import Select  from 'react-select'
import classNames from 'classnames'
import styles from './select.module.scss'

const CustomSelect = (props: Select['props']): JSX.Element => {
  const { className, ...selectProps } = props

  return (
    <Select 
      {...selectProps} 
      className={classNames('filter-select', styles.select, className)}
      classNamePrefix='filter-select'
    />
  )
}

export default CustomSelect