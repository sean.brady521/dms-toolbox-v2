import React, { useRef, useState } from 'react'
import classNames from 'classnames'
import styles from './section.module.scss'

import { SortableContainer, SortableElement, SortStart } from 'react-sortable-hoc'
import { Divider, Icon, Popup } from 'semantic-ui-react'

export type MenuOps = 'up' | 'down' | 'visible' | 'delete' | 'add'

export interface SectionProps {
  label?: string
  showPlaceholder?: boolean
  collapsed?: boolean
  itemName: string
  items: JSX.Element[]
  className?: string
  disableMenuOps?: MenuOps[]
  onChange: (oldIndex: number, newIndex: number) => void
  onMove: (direction: 'up' | 'down') => void,
  onDelete: () => void,
  onAddItem?: () => void,
  onChangeLabel: (label: string) => void
  onToggleCollapse: () => void
  onStartDrag?: (index: number) => void
}

const SortableItem = SortableElement(({value}: {value: JSX.Element}) => value)

const SortableList = SortableContainer(({ items }: { items: JSX.Element[] }) => {
  return (
    <div className={styles.listWrap}>
      {items.map((value, index) => (
        <SortableItem key={`item-${value.key}`} index={index} value={value} />
      ))}
    </div>
  )
})

const Section = (props: SectionProps): JSX.Element => {
  const { 
    itemName,
    label,
    items,
    className,
    showPlaceholder = true,
    disableMenuOps = [],
    collapsed,
    onChange,
    onToggleCollapse,
    onMove,
    onDelete,
    onAddItem,
    onStartDrag,
    onChangeLabel 
  } = props

  const [labelState, setLabelState] = useState<string | undefined>(label)

  const onSortEnd = ({ oldIndex, newIndex }: { oldIndex: number, newIndex: number }) => {
    onChange(oldIndex, newIndex)
  }

  const onSortStart = ({ index }: SortStart) => {
    onStartDrag && onStartDrag(index)
  }

  const labelRef = useRef<HTMLInputElement>(null)

  const handleSubmitLabel = () => {
    if (labelState === '') setLabelState('Untitled collection')
    onChangeLabel(labelState || 'Untitled collection')
    labelRef.current?.blur()
  }

  return (
    <div className={classNames(styles.root, className)}>
      {label !== undefined && 
        <div className={styles.topSectionWrap}>
          <input 
            className={styles.label} 
            value={labelState} 
            ref={labelRef}
            onChange={e => setLabelState(e.currentTarget.value)}
            autoFocus={labelState === ''}
            onKeyDown={e => {
              if (e.key === 'Enter' || e.keyCode === 13) {
                handleSubmitLabel()
              }
            }}
            onBlur={() => { 
              handleSubmitLabel()
            }}
          /> 
          <div className={styles.sectionMenu}>
            <Popup 
              content='Collapse collection' 
              trigger={
                <Icon 
                  circular 
                  disabled={disableMenuOps.includes('visible')}
                  name={collapsed ? 'eye' : 'hide'} 
                  className={styles.sectionMenuIcon} 
                  onClick={onToggleCollapse}
                />
              }
            />
            <Popup 
              content='Move collection up' 
              trigger={
                <Icon 
                  circular 
                  disabled={disableMenuOps.includes('up')}
                  name='angle up' 
                  className={styles.sectionMenuIcon} 
                  onClick={() => onMove('up')}
                />
              }
            />
            <Popup 
              content='Move collection down' 
              trigger={
                <Icon 
                  circular 
                  disabled={disableMenuOps.includes('down')}
                  name='angle down' 
                  className={styles.sectionMenuIcon} 
                  onClick={() => onMove('down')}
                />
              }
            />
            <Popup 
              content='Delete collection' 
              trigger={
                <Icon 
                  circular 
                  disabled={disableMenuOps.includes('delete')}
                  name='trash alternate' 
                  className={styles.sectionMenuIcon} 
                  onClick={onDelete}
                />
              }
            />
            {onAddItem && 
              <Popup 
                content={`Generate ${itemName} in collection`}
                trigger={
                  <Icon 
                    circular 
                    name='plus' 
                    disabled={disableMenuOps.includes('add')}
                    className={styles.sectionMenuIcon} 
                    onClick={onAddItem}
                  />
                }
              />
            }
          </div>
        </div>
      }
      {(items.length === 0 && showPlaceholder && !collapsed) &&
        <div className={styles.empty}>
          {`This collection is empty. Drag ${itemName}s here.`}
        </div>
      }
      {(items.length > 0 && !collapsed) &&
        <div>
          <SortableList 
            distance={3}
            items={items} 
            onSortEnd={onSortEnd} 
            onSortStart={onSortStart} 
            axis={'xy'} 
          />
        </div>
      }
      <Divider className={styles.divider} />
    </div>
  )
}

export default Section