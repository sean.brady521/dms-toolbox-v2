import React, { useState } from 'react'
import styles from './sectioned-grid.module.scss'
import Section from './section'
import { MenuOps } from './section/section'
import { Divider, Icon } from 'semantic-ui-react'

export interface Section {
  id: string
  showPlaceholder?: boolean
  collapsed?: boolean 
  label?: string
  items: JSX.Element[]
}

export interface SectionedGridProps {
  sections: Section[]
  itemName: string
  onChangeLabel: (secIdx: number, label: string) => void
  onChange: (oldSection: number, newSection: number, oldIndex: number, newIndex: number) => void
  onAddSection?: () => void
  onMoveSection: (oldIdx: number, newIdx: number) => void
  onDeleteSection: (id: string) => void
  onAddToSection?: (id: string) => void
  onToggleCollapseSection: (id: string) => void
}

const SectionedGrid = (props: SectionedGridProps): JSX.Element => {
  const { 
    sections, 
    itemName,
    onAddSection, 
    onChange, 
    onChangeLabel,
    onMoveSection,
    onDeleteSection,
    onAddToSection,
    onToggleCollapseSection
  } = props

  const [hoverSection, setHoverSection] = useState<number | null>(null)
  const [draggingItem, setDraggingItem] = useState<JSX.Element | null>(null)
  const [draggingItemSection, setDraggingItemSection] = useState<number | null>(null)

  const maybeAttachGhostItem = (idx: number, items: JSX.Element[]) => {
    const shouldAttach = idx === hoverSection && idx !== draggingItemSection && !!draggingItem
    return shouldAttach
      ? [...items, (<div key={0} className={styles.ghostItem}>{draggingItem}</div>)]
      : items
  }

  const getDisabledOps = (idx: number): MenuOps[] => {
    const ops: MenuOps[] = []
    if (idx === 1) ops.push('up')
    if (idx === sections.length - 1) ops.push('down')
    return ops
  }

  return ( 
    <div className={styles.root}>
      {!!onAddSection && 
        <Divider horizontal className={styles.addSection} onClick={onAddSection}>
          <div className={styles.addSectionIconWrap}>
            <Icon name='plus' className={styles.addSectionIcon} />
          </div>
        </Divider>
      }
      {sections.map((sec, idx) => ( 
        <div key={sec.id} onMouseEnter={() => setHoverSection(idx)}>
          <Section 
            className={styles.section}
            label={sec.label} 
            itemName={itemName}
            showPlaceholder={sec.showPlaceholder}
            collapsed={sec.collapsed}
            disableMenuOps={getDisabledOps(idx)}
            items={maybeAttachGhostItem(idx, sec.items)} 
            onChange={(oldIdx, newIdx) => { 
              onChange(idx, hoverSection ?? idx, oldIdx, newIdx)
              setDraggingItem(null)
            }}
            onChangeLabel={(label: string) => onChangeLabel(idx, label)}
            onStartDrag={index => { 
              setDraggingItem(sections[idx].items[index])
              setDraggingItemSection(idx)
            }}
            onMove={(direction: 'up' | 'down') => {
              const modifier = direction === 'up' ? -1 : 1
              onMoveSection(idx, idx + modifier)
            }}
            onDelete={() => onDeleteSection(sec.id)}
            onAddItem={onAddToSection ? () => onAddToSection(sec.id) : undefined}
            onToggleCollapse={() => onToggleCollapseSection(sec.id)}
          />
        </div>
      ))}

    </div>
  )
}

export default SectionedGrid