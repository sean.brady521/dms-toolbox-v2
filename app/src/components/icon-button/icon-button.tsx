import React, { ButtonHTMLAttributes } from 'react'
import { Icon, SemanticICONS } from 'semantic-ui-react'
import classNames from 'classnames'
import styles from './icon-button.module.scss'

export interface IconButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  icon: SemanticICONS
}

const IconButton = (props: IconButtonProps): JSX.Element => {
  const { className, icon, ...buttonProps } = props
  return (
    <button 
      className={classNames(className, styles.button)}
      {...buttonProps}
    >
      <Icon name={icon} className={styles.icon} />
    </button>
  )
}

export default IconButton