import React from 'react'

import classNames from 'classnames'
import { Input as SemInput, InputProps } from 'semantic-ui-react'

import styles from './input.module.scss'

const Input = (props: InputProps): JSX.Element => {
  const { className, ...otherProps } = props
  return (
    <SemInput 
      className={classNames(styles.input, className)} 
      {...otherProps} 
    />
  )
}

export default Input