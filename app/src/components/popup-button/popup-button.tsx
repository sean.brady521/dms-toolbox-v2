import React, { ReactNode } from 'react'
import { Button, Icon, Popup, PopupProps } from 'semantic-ui-react'
import StyledButton from 'components/button'
import styles from './popup-button.module.scss'
import { SemanticICONS } from 'semantic-ui-react/dist/commonjs/generic'

export interface PopupButtonProps {
  buttonText: string
  buttonIcon?: SemanticICONS
  labelPosition?: 'left' | 'right'
  className?: string
  open?: boolean
  position?: PopupProps['position']
  onClick?: () => void
  onOpen?: () => void
  onClose?: () => void
  children?: ReactNode
}

const PopupButton = (props: PopupButtonProps): JSX.Element => {
  const { buttonText, position, children, className, open, buttonIcon, labelPosition, onClick, onOpen, onClose } = props
  return (
    <Popup 
      on='click'
      basic
      position={position || 'bottom center'}
      className={styles.popup}
      onOpen={onOpen}
      onClose={onClose}
      open={open}
      trigger={(
        <StyledButton className={className} onClick={onClick}>
          {(!!buttonIcon && (
            <Icon name={buttonIcon} />
          ))}
          {buttonText}
        </StyledButton>
      )} 
    >
      {children}
    </Popup>
  )
}

export default PopupButton