import React, { useState } from 'react'

import { Icon } from 'semantic-ui-react'
import classNames from 'classnames'

import { alphaSortFn } from 'utils/sort'

import styles from './sortable-table.module.scss'

function getRowSorter (key: string, format: 'text' | 'number', dir: 'asc' | 'desc') {
  const mult = dir === 'asc' ? 1 : -1
  return format === 'text'
    // eslint-disable-next-line
    // @ts-ignore
    ? alphaSortFn((obj: Row) => obj.values[key] || '', mult)
    : (a: Row, b: Row) => ((a.values[key] as number) - (b.values[key] as number)) * mult
}

type SortDir = 'asc' | 'desc'

interface SortState {
  direction: SortDir,
  col: string
}

interface Row {
  id: string
  actions?: JSX.Element[]
  values: Record<string, string | number>
}

export interface SortableTableProps {
  columns: { value: string, label: string, format: 'text' | 'number' }[]
  rows: Row[]
  className?: string
  boldColumns?: string[]
  onClickRow: (id: string) => void
}

const SortableTable = (props: SortableTableProps): JSX.Element => {
  const {
    columns,
    rows,
    className,
    boldColumns,
    onClickRow
  } = props

  const [colSort, setColSort] = useState<SortState | null>(null)

  const sortingCol = columns.find(col => col.value === colSort?.col)
  const sortedRows = (!!sortingCol && !!colSort)
    ? rows.sort(getRowSorter(colSort.col, sortingCol.format, colSort.direction))
    : rows

  return (
    <div className={classNames(styles.root, className)}>
      <div className={styles.header}>
        {columns.map(col => (
          <div 
            key={col.value} 
            className={styles.colHeader}
            onClick={(): void => {
              const sort: SortState = {
                col: col.value,
                direction: 'asc'
              }

              if (colSort?.direction === 'asc') sort.direction = 'desc'

              setColSort(sort)
            }}
          >
            <div className={styles.colLabel}>
              {col.label}
            </div>
            <div className={styles.sortArrowWrap}>
              <Icon 
                name='caret up' 
                className={classNames(
                  styles.sortArrow, 
                  (colSort?.col === col.value && colSort.direction === 'desc') && styles.invis
                )} 
              />
              <Icon 
                name='caret down' 
                className={classNames(
                  styles.sortArrow, 
                  (colSort?.col === col.value && colSort.direction === 'asc') && styles.invis
                )} 
              />
            </div>
          </div>
        ))}
      </div>
      <div className={styles.rows}>
        {sortedRows.map(row => (
          <div 
            key={row.id} 
            className={styles.rowWrap}
            onClick={() => onClickRow(row.id)}
          >
            {columns.map(col => (
              <div 
                key={`${col.value}-${row.id}`} 
                className={classNames(styles.rowItem, boldColumns?.includes(col.value) && styles.bold)}
              >
                {row.values[col.value]}
              </div>
            ))}
            <div className={styles.actions}>
              {row.actions}
            </div>
          </div>
        ))}
      </div>
    </div>
  )
}

export default SortableTable