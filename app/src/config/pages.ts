export default [
  {
    value: 'home',
    label: 'Home',
    icon: 'home'
  },
  {
    value: 'npcs',
    label: 'NPCs',
    icon: 'group'
  },
  {
    value: 'encounters',
    label: 'Encounters',
    icon: 'fire'
  }
]
