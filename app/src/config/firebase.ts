export const firebaseConf = { 
  apiKey: 'AIzaSyDqQ1JOl63QmFc9xrlrXlWHXi2Y70i6-2c',
  authDomain: 'dms-toolbox-v2.firebaseapp.com',
  projectId: 'dms-toolbox-v2',
  storageBucket: 'dms-toolbox-v2.appspot.com',
  messagingSenderId: '576348514564',
  appId: '1:576348514564:web:6cbefa7404297dc4e40851'
}

export const actionCodeSettings = {
  url: process.env.NODE_ENV === 'development' 
    ? 'http://localhost:3000' 
    : 'https://dungeonmasters.tools',
  handleCodeInApp: true,
}