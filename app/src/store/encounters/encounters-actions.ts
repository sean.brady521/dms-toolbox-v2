import { IEncounter } from '../../types/encounters'
import { fetchAll, addOrModifyEntry, removeById, Pojo, fetch5eMonsters } from 'utils/server'
import { Dispatch, Store } from 'redux'
import { MonsterData } from 'types/monster'

const _removeEncounterBegin = (id: string) => ({
  type: 'REMOVE_ENCOUNTER_BEGIN',
  payload: { id }
})

const _removeEncounterSuccess = () => ({
  type: 'REMOVE_ENCOUNTER_SUCCESS',
})

const _removeEncounterFailure = (error: Error) => ({
  type: 'REMOVE_ENCOUNTER_FAILURE',
  payload: { error }
})

const _postEncounterBegin = (encounter: Partial<IEncounter>) => ({
  type: 'POST_ENCOUNTER_BEGIN',
  payload: { encounter }
})

const _postEncounterSuccess = () => ({
  type: 'POST_ENCOUNTER_SUCCESS',
})

const _postEncounterFailure = (error: Error) => ({
  type: 'POST_ENCOUNTER_FAILURE',
  payload: { error }
})

const _fetchEncountersBegin = () => ({
  type: 'FETCH_ENCOUNTERS_BEGIN',
})

const _fetchEncountersSuccess = (encounters: IEncounter[]) => ({
  type: 'FETCH_ENCOUNTERS_SUCCESS',
  payload: { encounters }
})

const _fetchEncountersFailure = (error: Error) => ({
  type: 'FETCH_ENCOUNTERS_FAILURE',
  payload: { error }
})

const _fetchCreaturesBegin = () => ({
  type: 'FETCH_CREATURES_BEGIN',
})

const _fetchCreaturesSuccess = (creatures: MonsterData) => ({
  type: 'FETCH_CREATURES_SUCCESS',
  payload: { creatures }
})

const _fetchCreaturesFailure = (error: Error) => ({
  type: 'FETCH_CREATURES_FAILURE',
  payload: { error }
})

export const removeEncounter = (id: string) => {
  return (dispatch: Dispatch): Promise<void> => {
    const onSuccess = () => dispatch(_removeEncounterSuccess())
    const onFailure = (err: Error) => dispatch(_removeEncounterFailure(err))

    dispatch(_removeEncounterBegin(id))
  
    return removeById('encounter', id, onSuccess, onFailure)
  }
}

export const postEncounter = (encounter: Partial<IEncounter>) => {
  return (dispatch: Dispatch): Promise<void> => {
    const onSuccess = () => dispatch(_postEncounterSuccess())
    const onFailure = (err: Error) => dispatch(_postEncounterFailure(err))

    dispatch(_postEncounterBegin(encounter))

    return addOrModifyEntry('encounter', encounter, onSuccess, onFailure)
  }
}

export const fetchEncounters = () => {
  return (dispatch: Dispatch): Promise<void> => {

    const onSuccess = (encounters?: Pojo) => dispatch(_fetchEncountersSuccess(encounters as unknown as IEncounter[]))
    const onFailure = (error: Error) => dispatch(_fetchEncountersFailure(error))

    dispatch(_fetchEncountersBegin())
    return fetchAll('encounters', onSuccess, onFailure)
  }
}

export const fetchCreatures = (creatures: string[]) => {
  return (dispatch: Dispatch): Promise<void> => {
    const onSuccess = (creaturesData?: Pojo) => dispatch(_fetchCreaturesSuccess(creaturesData as unknown as MonsterData))
    const onFailure = (error: Error) => dispatch(_fetchCreaturesFailure(error))

    dispatch(_fetchCreaturesBegin())
    return fetch5eMonsters(creatures, onSuccess, onFailure)
  }
}
