import { EncountersState } from 'types/common'
import { IEncounter } from 'types/encounters'
import { MonsterData } from 'types/monster'
import { deepClone } from 'utils/clone'
import {
  REMOVE_ENCOUNTER_BEGIN,
  REMOVE_ENCOUNTER_SUCCESS,
  REMOVE_ENCOUNTER_FAILURE,
  POST_ENCOUNTER_BEGIN,
  POST_ENCOUNTER_SUCCESS,
  POST_ENCOUNTER_FAILURE,
  FETCH_ENCOUNTERS_BEGIN,
  FETCH_ENCOUNTERS_SUCCESS,
  FETCH_ENCOUNTERS_FAILURE,
  FETCH_CREATURES_BEGIN,
  FETCH_CREATURES_FAILURE,
  FETCH_CREATURES_SUCCESS,
  EncounterActionTypes
} from './encounters-action-types'

const initialState: EncountersState = {
  encounters: {},
  creatureCache: {},
  loading: false,
  fetchingCreatures: false,
  hasFetched: false,
  error: null,
}

function updateOrAdd<T extends { id: string }>(curr: Record<string, T>, inc: T): Record<string, T> {
  return {
    ...curr,
    [inc.id]: inc
  }
}

function deleteEncounter (encounters: Record<string, IEncounter>, id: string) {
  const newEncounters = deepClone(encounters)
  delete newEncounters[id]
  return newEncounters
}

export default (
  state = initialState,
  action: EncounterActionTypes
): EncountersState => {
  switch (action.type) {
  case REMOVE_ENCOUNTER_BEGIN:
    return {
      ...state,
      encounters: deleteEncounter(state.encounters, action.payload.id),
      loading: true
    }
  case REMOVE_ENCOUNTER_SUCCESS:
    return {
      ...state,
      loading: false
    }
  case REMOVE_ENCOUNTER_FAILURE:
    return {
      ...state,
      error: action.payload.error,
      loading: false
    }
  case POST_ENCOUNTER_BEGIN:
    return {
      ...state,
      encounters: updateOrAdd(state.encounters, action.payload.encounter),
      loading: true
    }
  case POST_ENCOUNTER_SUCCESS:
    return {
      ...state,
    }
  case POST_ENCOUNTER_FAILURE:
    return {
      ...state,
      error: action.payload.error,
      loading: false
    }
  case FETCH_CREATURES_BEGIN:
    return {
      ...state,
      fetchingCreatures: true,
      error: null
    }
  case FETCH_CREATURES_SUCCESS:
    return {
      ...state,
      fetchingCreatures: false,
      creatureCache: { ...state.creatureCache, ...action.payload.creatures },
      hasFetched: true
    }
  case FETCH_CREATURES_FAILURE:
    return {
      ...state,
      fetchingCreatures: false,
      error: action.payload.error,
      hasFetched: true,
      encounters: {}
    }
  case FETCH_ENCOUNTERS_BEGIN:
    return {
      ...state,
      loading: true,
      error: null
    }
  case FETCH_ENCOUNTERS_SUCCESS:
    return {
      ...state,
      loading: false,
      encounters: action.payload.encounters,
      hasFetched: true
    }
  case FETCH_ENCOUNTERS_FAILURE:
    return {
      ...state,
      loading: false,
      error: action.payload.error,
      hasFetched: true,
      encounters: {}
    }
  default:
    return state
  }
}