import { ISection } from 'types/common'
import { IEncounter, EncounterSection } from 'types/encounters'

export function selectEncountersBySection (encounters: Record<string, IEncounter>, sections: ISection[]): EncounterSection[] {
  const encounterSections: EncounterSection[] = []
  for (const sec of sections) {
    const sectionEncounters: IEncounter[] = []
    for (const encounterId of sec.itemIds) {
      if (!encounters[encounterId]) continue
      sectionEncounters.push(encounters[encounterId])
    }
    encounterSections.push({
      id: sec.id,
      showPlaceholder: !sec.isRoot,
      collapsed: sec.collapsed,
      label: sec.label,
      encounters: sectionEncounters
    })
  }

  return encounterSections
}