import { IEncounter } from 'types/encounters'
import { MonsterData } from 'types/monster'

export const REMOVE_ENCOUNTER_BEGIN = 'REMOVE_ENCOUNTER_BEGIN'
export const REMOVE_ENCOUNTER_SUCCESS = 'REMOVE_ENCOUNTER_SUCCESS'
export const REMOVE_ENCOUNTER_FAILURE = 'REMOVE_ENCOUNTER_FAILURE'
export const POST_ENCOUNTER_BEGIN = 'POST_ENCOUNTER_BEGIN'
export const POST_ENCOUNTER_SUCCESS = 'POST_ENCOUNTER_SUCCESS'
export const POST_ENCOUNTER_FAILURE = 'POST_ENCOUNTER_FAILURE'
export const FETCH_ENCOUNTERS_BEGIN = 'FETCH_ENCOUNTERS_BEGIN'
export const FETCH_ENCOUNTERS_SUCCESS = 'FETCH_ENCOUNTERS_SUCCESS'
export const FETCH_ENCOUNTERS_FAILURE = 'FETCH_ENCOUNTERS_FAILURE'
export const FETCH_CREATURES_BEGIN = 'FETCH_CREATURES_BEGIN'
export const FETCH_CREATURES_SUCCESS = 'FETCH_CREATURES_SUCCESS'
export const FETCH_CREATURES_FAILURE = 'FETCH_CREATURES_FAILURE'

interface ErrorPayloadAction {
  type: typeof REMOVE_ENCOUNTER_FAILURE | typeof POST_ENCOUNTER_FAILURE | typeof FETCH_ENCOUNTERS_FAILURE | typeof FETCH_CREATURES_FAILURE
  payload: {
    error: string
  }
}

interface IdPayloadAction {
  type: typeof REMOVE_ENCOUNTER_BEGIN
  payload: {
    id: string
  }
}

interface NoPayloadAction {
  type: typeof REMOVE_ENCOUNTER_SUCCESS | typeof POST_ENCOUNTER_SUCCESS | typeof FETCH_ENCOUNTERS_BEGIN | typeof FETCH_CREATURES_BEGIN
}

interface EncounterPayloadAction {
  type: typeof POST_ENCOUNTER_BEGIN
  payload: {
    encounter: IEncounter
  }
}

interface EncountersPayloadAction {
  type: typeof FETCH_ENCOUNTERS_SUCCESS
  payload: {
    encounters: Record<string, IEncounter>
  }
}

interface CreaturesPayloadAction {
  type: typeof FETCH_CREATURES_SUCCESS
  payload: {
    creatures: Record<string, MonsterData>
  }
}


export type EncounterActionTypes = ErrorPayloadAction | IdPayloadAction | NoPayloadAction | EncounterPayloadAction | EncountersPayloadAction | CreaturesPayloadAction