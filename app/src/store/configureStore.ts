import { createStore, combineReducers, applyMiddleware, compose, Store } from 'redux'
import thunk from 'redux-thunk'
import { createBrowserHistory } from 'history'
import { connectRouter, routerMiddleware } from 'connected-react-router'

import userReducer from './user/user-reducers'

import { StoreState } from 'types/common'
import npcsReducers from './npcs/npcs-reducers'
import encounterReducers from './encounters/encounters-reducers'
import sectionsReducers from './sections/sections-reducers'

declare global { interface Window { __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose } }

export const history = createBrowserHistory()

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const initialState = {}

const configureStore = (): Store => {
  const store = createStore(
    combineReducers<StoreState>({ 
      router: connectRouter(history), 
      user: userReducer,
      npcs: npcsReducers,
      encounters: encounterReducers,
      sections: sectionsReducers
    }),
    initialState,
    composeEnhancers( applyMiddleware(thunk, routerMiddleware(history)))
  )

  return store
}

export default configureStore