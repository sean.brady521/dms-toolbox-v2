import { INpc } from '../../types/npcs'
import { fetchAll, addOrModifyEntry, removeById, Pojo } from 'utils/server'
import { Dispatch } from 'redux'

const _removeNpcBegin = (id: string) => ({
  type: 'REMOVE_NPC_BEGIN',
  payload: { id }
})

const _removeNpcSuccess = () => ({
  type: 'REMOVE_NPC_SUCCESS',
})

const _removeNpcFailure = (error: Error) => ({
  type: 'REMOVE_NPC_FAILURE',
  payload: { error }
})

const _postNpcBegin = (npc: Partial<INpc>) => ({
  type: 'POST_NPC_BEGIN',
  payload: { npc }
})

const _postNpcSuccess = () => ({
  type: 'POST_NPC_SUCCESS',
})

const _postNpcFailure = (error: Error) => ({
  type: 'POST_NPC_FAILURE',
  payload: { error }
})

const _fetchNpcsBegin = () => ({
  type: 'FETCH_NPCS_BEGIN',
})

const _fetchNpcsSuccess = (npcs: INpc[]) => ({
  type: 'FETCH_NPCS_SUCCESS',
  payload: { npcs }
})

const _fetchNpcsFailure = (error: Error) => ({
  type: 'FETCH_NPCS_FAILURE',
  payload: { error }
})

export const removeNpc = (id: string) => {
  return (dispatch: Dispatch): Promise<void> => {
    const onSuccess = () => dispatch(_removeNpcSuccess())
    const onFailure = (err: Error) => dispatch(_removeNpcFailure(err))

    dispatch(_removeNpcBegin(id))
  
    return removeById('npc', id, onSuccess, onFailure)
  }
}

export const postNpc = (npc: Partial<INpc>) => {
  return (dispatch: Dispatch): Promise<void> => {
    const onSuccess = () => dispatch(_postNpcSuccess())
    const onFailure = (err: Error) => dispatch(_postNpcFailure(err))

    dispatch(_postNpcBegin(npc))

    return addOrModifyEntry('npc', npc, onSuccess, onFailure)
  }
}

export const fetchNpcs = () => {
  return (dispatch: Dispatch): Promise<void> => {

    const onSuccess = (npcs?: Pojo) => dispatch(_fetchNpcsSuccess(npcs as unknown as INpc[]))
    const onFailure = (error: Error) => dispatch(_fetchNpcsFailure(error))

    dispatch(_fetchNpcsBegin())
    return fetchAll('npcs', onSuccess, onFailure)
  }
}
