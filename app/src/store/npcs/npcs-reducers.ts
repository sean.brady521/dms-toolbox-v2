import { NpcsState } from 'types/common'
import { INpc } from 'types/npcs'
import { deepClone } from 'utils/clone'
import {
  REMOVE_NPC_BEGIN,
  REMOVE_NPC_SUCCESS,
  REMOVE_NPC_FAILURE,
  POST_NPC_BEGIN,
  POST_NPC_SUCCESS,
  POST_NPC_FAILURE,
  FETCH_NPCS_BEGIN,
  FETCH_NPCS_SUCCESS,
  FETCH_NPCS_FAILURE,
  NpcActionTypes
} from './npcs-action-types'

const initialState: NpcsState = {
  npcs: {},
  loading: false,
  hasFetched: false,
  error: null,
}

function updateOrAdd(currNpcs: Record<string, INpc>, incNpc: INpc): Record<string, INpc> {
  return {
    ...currNpcs,
    [incNpc.id]: incNpc
  }
}

function deleteNpc (npcs: Record<string, INpc>, id: string) {
  const newNpcs = deepClone(npcs)
  delete newNpcs[id]
  return newNpcs
}

export default (
  state = initialState,
  action: NpcActionTypes
): NpcsState => {
  switch (action.type) {
  case REMOVE_NPC_BEGIN:
    return {
      ...state,
      npcs: deleteNpc(state.npcs, action.payload.id),
      loading: true
    }
  case REMOVE_NPC_SUCCESS:
    return {
      ...state,
      loading: false
    }
  case REMOVE_NPC_FAILURE:
    return {
      ...state,
      error: action.payload.error,
      loading: false
    }
  case POST_NPC_BEGIN:
    return {
      ...state,
      npcs: updateOrAdd(state.npcs, action.payload.npc),
      loading: true
    }
  case POST_NPC_SUCCESS:
    return {
      ...state,
    }
  case POST_NPC_FAILURE:
    return {
      ...state,
      error: action.payload.error,
      loading: false
    }
  case FETCH_NPCS_BEGIN:
    return {
      ...state,
      loading: true,
      error: null
    }
  case FETCH_NPCS_SUCCESS:
    return {
      ...state,
      loading: false,
      npcs: action.payload.npcs,
      hasFetched: true
    }
  case FETCH_NPCS_FAILURE:
    return {
      ...state,
      loading: false,
      error: action.payload.error,
      hasFetched: true,
      npcs: {}
    }
  default:
    return state
  }
}