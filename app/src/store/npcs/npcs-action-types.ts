import { INpc } from 'types/npcs'

export const REMOVE_NPC_BEGIN = 'REMOVE_NPC_BEGIN'
export const REMOVE_NPC_SUCCESS = 'REMOVE_NPC_SUCCESS'
export const REMOVE_NPC_FAILURE = 'REMOVE_NPC_FAILURE'
export const POST_NPC_BEGIN = 'POST_NPC_BEGIN'
export const POST_NPC_SUCCESS = 'POST_NPC_SUCCESS'
export const POST_NPC_FAILURE = 'POST_NPC_FAILURE'
export const FETCH_NPCS_BEGIN = 'FETCH_NPCS_BEGIN'
export const FETCH_NPCS_SUCCESS = 'FETCH_NPCS_SUCCESS'
export const FETCH_NPCS_FAILURE = 'FETCH_NPCS_FAILURE'

interface ErrorPayloadAction {
  type: typeof REMOVE_NPC_FAILURE | typeof POST_NPC_FAILURE | typeof FETCH_NPCS_FAILURE
  payload: {
    error: string
  }
}

interface IdPayloadAction {
  type: typeof REMOVE_NPC_BEGIN
  payload: {
    id: string
  }
}

interface NoPayloadAction {
  type: typeof REMOVE_NPC_SUCCESS | typeof POST_NPC_SUCCESS | typeof FETCH_NPCS_BEGIN
}

interface NpcPayloadAction {
  type: typeof POST_NPC_BEGIN
  payload: {
    npc: INpc
  }
}

interface NpcsPayloadAction {
  type: typeof FETCH_NPCS_SUCCESS
  payload: {
    npcs: Record<string, INpc>
  }
}


export type NpcActionTypes = ErrorPayloadAction | IdPayloadAction | NoPayloadAction | NpcPayloadAction | NpcsPayloadAction