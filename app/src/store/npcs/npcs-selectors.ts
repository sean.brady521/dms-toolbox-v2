import { ISection } from 'types/common'
import { INpc, NpcSection } from 'types/npcs'

export function selectNpcsBySection (npcs: Record<string, INpc>, sections: ISection[]): NpcSection[] {
  const npcSections: NpcSection[] = []
  for (const sec of sections) {
    const sectionNpcs: INpc[] = []
    for (const npcId of sec.itemIds) {
      if (!npcs[npcId]) continue
      sectionNpcs.push(npcs[npcId])
    }
    npcSections.push({
      id: sec.id,
      showPlaceholder: !sec.isRoot,
      collapsed: sec.collapsed,
      label: sec.label,
      npcs: sectionNpcs
    })
  }

  return npcSections
}