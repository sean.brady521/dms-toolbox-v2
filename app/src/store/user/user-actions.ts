import firebase from 'firebase/app'
import 'firebase/auth'
import { checkEmailSignIn } from 'utils/auth'
import { Dispatch } from 'redux'
import { replace } from 'connected-react-router'
import { SetAnonymousAction, SetErrorAction, SetLoadingAction, SetLoggedInAction } from './user-action-types'

export const setLoggedIn = (loggedIn: boolean): SetLoggedInAction => ({
  type: 'SET_LOGGED_IN',
  payload: { loggedIn }
})

export const setLoading = (loading: boolean): SetLoadingAction => ({
  type: 'SET_LOADING',
  payload: { loading }
})

export const setError = (error: string): SetErrorAction => ({
  type: 'SET_ERROR',
  payload: { error }
})

export const setAnonymous = (anonymous: boolean): SetAnonymousAction => ({
  type: 'SET_ANONYMOUS',
  payload: { anonymous }
})

export const initAuthManager = (onLogin?: () => void) => {
  return async (dispatch: Dispatch): Promise<void> => {
    await checkEmailSignIn(() => { 
      dispatch(replace('/'))
    })
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        dispatch(setAnonymous(user.isAnonymous))
        dispatch(setLoggedIn(!!user))
        if(onLogin) onLogin()
        dispatch(setLoading(false))
      } else {
        firebase.auth().signInAnonymously()
          .catch((err: Error) => {
            throw err
          })
      }
    })
  }
}
