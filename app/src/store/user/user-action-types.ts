export const SET_LOGGED_IN = 'SET_LOGGED_IN'
export const SET_LOADING = 'SET_LOADING'
export const SET_ERROR = 'SET_ERROR'
export const SET_ANONYMOUS = 'SET_ANONYMOUS'
export const LOGOUT = 'LOGOUT'

export interface SetLoggedInAction {
  type: typeof SET_LOGGED_IN
  payload: {
    loggedIn: boolean
  }
}

export interface SetLoadingAction {
  type: typeof SET_LOADING
  payload: {
    loading: boolean
  }
}

export interface SetAnonymousAction {
  type: typeof SET_ANONYMOUS
  payload: {
    anonymous: boolean
  }
}

export interface SetErrorAction {
  type: typeof SET_ERROR
  payload: {
    error: string
  }
}

export type UserActionTypes = SetLoggedInAction | SetLoadingAction | SetErrorAction | SetAnonymousAction