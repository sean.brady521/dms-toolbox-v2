import { UserActionTypes } from './user-action-types'
import { UserState } from 'types/common'

const initialState: UserState = {
  loading: true,
  loggedIn: false,
  anonymous: false,
  error: null
}

export default (
  state = initialState,
  action: UserActionTypes
): UserState => {
  switch (action.type) {
  case 'SET_LOGGED_IN':
    return {
      ...state,
      loggedIn: action.payload.loggedIn
    }
  case 'SET_LOADING':
    return {
      ...state,
      loading: action.payload.loading
    }
  case 'SET_ERROR':
    return {
      ...state,
      error: action.payload.error
    }
  case 'SET_ANONYMOUS':
    return {
      ...state,
      anonymous: action.payload.anonymous
    }
  default:
    return state
  }
}
