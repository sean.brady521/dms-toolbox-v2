import { SectionsState } from 'types/common'
import { ISection } from 'types/common'
import {
  REMOVE_SECTION_BEGIN,
  REMOVE_SECTION_SUCCESS,
  REMOVE_SECTION_FAILURE,
  POST_SECTIONS_BEGIN,
  POST_SECTIONS_SUCCESS,
  POST_SECTIONS_FAILURE,
  FETCH_SECTIONS_BEGIN,
  FETCH_SECTIONS_SUCCESS,
  FETCH_SECTIONS_FAILURE,
  SectionActionTypes
} from './sections-action-types'

const initialState: SectionsState = {
  sections: [],
  loading: false,
  hasFetched: false,
  error: null,
}

function updateOrAdd(currSections: ISection[], incSections: ISection[]): ISection[] {
  const newSections = [...currSections]

  for (const sec of incSections ) {
    let found = false
    for (let i = 0 ; i < newSections.length ; i++) {
      if (newSections[i].id === sec.id) {
        newSections[i] = sec
        found = true
        break
      }
    }
    if (!found) newSections.push(sec)
  }
  return newSections
}

export default (
  state = initialState,
  action: SectionActionTypes
): SectionsState => {
  switch (action.type) {
  case REMOVE_SECTION_BEGIN:
    return {
      ...state,
      sections: state.sections.filter(section => section.id !== action.payload.id),
      loading: true
    }
  case REMOVE_SECTION_SUCCESS:
    return {
      ...state,
      loading: false
    }
  case REMOVE_SECTION_FAILURE:
    return {
      ...state,
      error: action.payload.error,
      loading: false
    }
  case POST_SECTIONS_BEGIN:
    return {
      ...state,
      sections: updateOrAdd(state.sections, action.payload.sections),
      loading: true
    }
  case POST_SECTIONS_SUCCESS:
    return {
      ...state,
    }
  case POST_SECTIONS_FAILURE:
    return {
      ...state,
      error: action.payload.error,
      loading: false
    }
  case FETCH_SECTIONS_BEGIN:
    return {
      ...state,
      loading: true,
      error: null
    }
  case FETCH_SECTIONS_SUCCESS:
    return {
      ...state,
      loading: false,
      sections: action.payload.sections,
      hasFetched: true
    }
  case FETCH_SECTIONS_FAILURE:
    return {
      ...state,
      loading: false,
      error: action.payload.error,
      hasFetched: true,
      sections: []
    }
  default:
    return state
  }
}