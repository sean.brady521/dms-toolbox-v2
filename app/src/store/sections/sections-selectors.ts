import { ISection, SectionScope } from 'types/common'

export function selectScopedSections (sections: ISection[], scope: SectionScope): ISection[] {
  return sections.filter(sec => sec.scope === scope)
}

export function selectOrderedSections (sections: ISection[]): ISection[] {
  return sections.sort((a, b) => a.rank - b.rank)
}