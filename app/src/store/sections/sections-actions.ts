import { ISection } from '../../types/common'
import { fetchAll, addOrModifyEntry, removeById, Pojo } from 'utils/server'
import { Dispatch } from 'redux'

const _removeSectionBegin = (id: string) => ({
  type: 'REMOVE_SECTION_BEGIN',
  payload: { id }
})

const _removeSectionSuccess = () => ({
  type: 'REMOVE_SECTION_SUCCESS',
})

const _removeSectionFailure = (error: Error) => ({
  type: 'REMOVE_SECTION_FAILURE',
  payload: { error }
})

const _postSectionsBegin = (sections: ISection[]) => ({
  type: 'POST_SECTIONS_BEGIN',
  payload: { sections }
})

const _postSectionsSuccess = () => ({
  type: 'POST_SECTIONS_SUCCESS',
})

const _postSectionsFailure = (error: Error) => ({
  type: 'POST_SECTIONS_FAILURE',
  payload: { error }
})

const _fetchSectionsBegin = () => ({
  type: 'FETCH_SECTIONS_BEGIN',
})

const _fetchSectionsSuccess = (sections: ISection[]) => ({
  type: 'FETCH_SECTIONS_SUCCESS',
  payload: { sections }
})

const _fetchSectionsFailure = (error: Error) => ({
  type: 'FETCH_SECTIONS_FAILURE',
  payload: { error }
})

export const removeSection = (id: string) => {
  return (dispatch: Dispatch): Promise<void> => {
    const onSuccess = () => dispatch(_removeSectionSuccess())
    const onFailure = (err: Error) => dispatch(_removeSectionFailure(err))

    dispatch(_removeSectionBegin(id))
  
    return removeById('section', id, onSuccess, onFailure)
  }
}

export const postSections = (sections: ISection[]) => {
  return (dispatch: Dispatch): Promise<void> => {
    const onSuccess = () => dispatch(_postSectionsSuccess())
    const onFailure = (err: Error) => dispatch(_postSectionsFailure(err))

    dispatch(_postSectionsBegin(sections))

    return addOrModifyEntry('sections', sections as unknown as Pojo, onSuccess, onFailure)
  }
}

export const fetchSections = () => {
  return (dispatch: Dispatch): Promise<void> => {

    const onSuccess = (sections?: Pojo) => dispatch(_fetchSectionsSuccess(sections as unknown as ISection[]))
    const onFailure = (error: Error) => dispatch(_fetchSectionsFailure(error))

    dispatch(_fetchSectionsBegin())
    return fetchAll('sections', onSuccess, onFailure)
  }
}

export const addRecToSection = (recId: string, section: ISection): (dispatch: Dispatch) => void => {
  if (section.itemIds.includes(recId)) return () => { return }
  const newSection: ISection = {...section, itemIds: [...section.itemIds, recId]}
  return (dispatch: Dispatch) => postSections([newSection])(dispatch)
}

export const removeRecFromSection = (recId: string, section: ISection): (dispatch: Dispatch) => void => {
  if (!section.itemIds.includes(recId)) return () => { return }
  const newSection: ISection = {...section, itemIds: section.itemIds.filter(id => id !== recId)}
  return (dispatch: Dispatch) => postSections([newSection])(dispatch)
}
