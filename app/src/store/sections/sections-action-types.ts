import { ISection } from 'types/common'

export const REMOVE_SECTION_BEGIN = 'REMOVE_SECTION_BEGIN'
export const REMOVE_SECTION_SUCCESS = 'REMOVE_SECTION_SUCCESS'
export const REMOVE_SECTION_FAILURE = 'REMOVE_SECTION_FAILURE'
export const POST_SECTIONS_BEGIN = 'POST_SECTIONS_BEGIN'
export const POST_SECTIONS_SUCCESS = 'POST_SECTIONS_SUCCESS'
export const POST_SECTIONS_FAILURE = 'POST_SECTIONS_FAILURE'
export const FETCH_SECTIONS_BEGIN = 'FETCH_SECTIONS_BEGIN'
export const FETCH_SECTIONS_SUCCESS = 'FETCH_SECTIONS_SUCCESS'
export const FETCH_SECTIONS_FAILURE = 'FETCH_SECTIONS_FAILURE'

interface ErrorPayloadAction {
  type: typeof REMOVE_SECTION_FAILURE | typeof POST_SECTIONS_FAILURE | typeof FETCH_SECTIONS_FAILURE
  payload: {
    error: string
  }
}

interface IdPayloadAction {
  type: typeof REMOVE_SECTION_BEGIN
  payload: {
    id: string
  }
}

interface NoPayloadAction {
  type: typeof REMOVE_SECTION_SUCCESS | typeof POST_SECTIONS_SUCCESS | typeof FETCH_SECTIONS_BEGIN
}

interface SectionsPayloadAction {
  type: typeof FETCH_SECTIONS_SUCCESS | typeof POST_SECTIONS_BEGIN
  payload: {
    sections: ISection[]
  }
}


export type SectionActionTypes = ErrorPayloadAction | IdPayloadAction | NoPayloadAction | SectionsPayloadAction