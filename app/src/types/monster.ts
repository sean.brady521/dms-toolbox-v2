import { MonsterSize } from "constants/enums";

export type CreatureSize = 'Fine' | 'Diminutive' | 'Tiny'
| 'Small' | 'Medium' | 'Large'
| 'Huge' | 'Gargantuan' | 'Colossal'

export interface MonsterSpeed {
  [key: string]: string
}

export interface APIResource {
  index: string | number
  name: string
  url: string
}

export interface ProficiencyBonus {
  value: number
  proficiency: {
    index: APIResource['index']
    url: APIResource['url']
    name: string
  }
}

export interface SpecialAbility {
  name: string
  desc: string
  dc?: DifficultyClass
}

export interface Action {
  name: string
  desc: string
  usage?: ActionUsage
}
export interface ActionUsage {
  type: string
  times: number
}


export interface DifficultyClass {
  dc_type: APIResource
  dc_value: number
  success_type: string
}

export interface MonsterData {
  name: string
  index: string | number
  url: string
  size: CreatureSize
  type: string
  subtype: string
  alignment: string
  xp: number,
  armor_class: number
  hit_points: number
  hit_dice: string
  speed: MonsterSpeed
  strength: number
  dexterity: number
  constitution: number
  intelligence: number
  wisdom: number
  charisma: number
  proficiencies: ProficiencyBonus[]
  damage_vulnerabilities: APIResource[]
  damage_resistances: APIResource[]
  damage_immunities: APIResource[]
  condition_immunities: APIResource[]
  senses: {
    darkvision: string
    passive_perception: number
  }
  languages: string
  challenge_rating: number
  special_abilities: SpecialAbility[]
  actions: Action[]
  reactions: Action[]
  legendary_actions: Action[]
}

export interface SimpleMonster {
  name: string
  index: string
  type: string
  size: string
  alignment: string
  environments: string[]
  cr: number
  xp: number
}

export type LawVsChaos = 'lawful' | 'neutral' | 'chaotic'
export type GoodVsEvil = 'good' | 'neutral' | 'evil'

export interface SearchQuery {
  q?: string
  challenge?: {
    min: number
    max: number
  }
  type?: string[]
  environments?: string[]
  alignment?: {
    lawVsChaos: LawVsChaos[]
    goodVsEvil: GoodVsEvil[]
  }
  size?: {
    min: MonsterSize
    max: MonsterSize
  }
}
