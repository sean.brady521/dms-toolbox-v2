import { RouterState } from 'connected-react-router'
import { INpc } from './npcs'
import { IEncounter } from './encounters'
import { SemanticICONS } from 'semantic-ui-react/dist/commonjs/generic'
import { MonsterData } from './monster'

export interface StoreState {
  router: RouterState
  user: UserState
  npcs: NpcsState
  encounters: EncountersState
  sections: SectionsState
}

export interface UserState {
  loading: boolean
  loggedIn: boolean
  error: string | null
  anonymous: boolean
}

export interface NpcsState {
  npcs: Record<string, INpc>
  error: string | null
  loading: boolean
  hasFetched: boolean
}

export interface EncountersState {
  encounters: Record<string, IEncounter>
  creatureCache: Record<string, MonsterData>
  fetchingCreatures: boolean
  error: string | null
  loading: boolean
  hasFetched: boolean
}

export interface SectionsState {
  sections: ISection[]
  error: string | null
  loading: boolean
  hasFetched: boolean
}


export interface Location {
  pathname: string
  search: string
  hash: string
}

export interface Option {
  value: string
  label: string
  icon?: SemanticICONS
}

export type SectionScope = 'npcs' | 'encounters'

export interface ISection {
  id: string
  createdBy: string
  rank: number
  collapsed?: boolean
  scope: SectionScope
  isRoot?: boolean
  label?: string
  itemIds: string[]
}