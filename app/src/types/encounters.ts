import { Environment, Difficulty } from 'constants/enums'

export interface IEncounter {
  id: string
  name: string
  manual?: boolean
  config: EncounterConfig
  createdBy: string
  modifiedAt: number
  createdAt: number
  creatures: ECreature[]
  personality?: string
  relationship?: string
}

export interface EncounterConfig {
  avgLvl: number
  numPcs: number
  environment?: Environment
  difficulty?: Difficulty
}

export interface ECreature {
  count: number
  creature: string
}

export interface Personality {
  type: string
  description: string
}

export interface Relationship {
  type: string
  description: string
  joiner: string
}

export interface EncounterSection {
  id: string
  showPlaceholder?: boolean
  collapsed?: boolean
  label?: string
  encounters: IEncounter[]
}
